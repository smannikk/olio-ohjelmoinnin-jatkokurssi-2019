#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>
#include <random>

int rand(int min, int max)
{
    static thread_local std::mt19937 prng;
    std::uniform_int_distribution<int> dist(min, max);
    return dist(prng);
}

std::mutex lock;

void sayHelloThread(const std::string &s)
{
    for (int i=0; i<10; ++i)
    {
        lock.lock();
        std::cout << s << "\n";
        lock.unlock();

        // Wait a random amount of time to simulate a delay.
        std::this_thread::sleep_for(std::chrono::milliseconds(rand(1, 10)));
    }
}

int main()
{
    std::thread t1(sayHelloThread, "Hello world!");
    std::thread t2(sayHelloThread, "Moro maailma!");

    t1.join();
    t2.join();

    return 0;
}
