#ifndef F_GREETER_HPP_INCLUDED
#define F_GREETER_HPP_INCLUDED

#include <string>
#include <vector>

class Greeter
{
private:
    std::vector<std::string*> greetings;
public:
    Greeter(const std::string &g);

    Greeter(const Greeter &other);
    Greeter(Greeter &&other);

    Greeter& operator=(const Greeter &other);
    Greeter& operator=(Greeter &&other);

    void swap(Greeter &other);

    ~Greeter();

    void addGreet(const std::string &g);
    std::string sayHello() const;
};

#endif // F_GREETER_HPP_INCLUDED
