#include "greeter.hpp"

#include <utility>

Greeter::Greeter(const std::string &g)
{
    greetings.push_back(new std::string(g));
}

Greeter::Greeter(const Greeter &other)
{
    greetings.reserve(other.greetings.size());

    for (std::string* const g : other.greetings)
        greetings.push_back(new std::string(*g));
}

Greeter::Greeter(Greeter &&other) : greetings(std::move(other.greetings)) {}

Greeter& Greeter::operator=(const Greeter &other)
{
    if (this != &other)
    {
        for (std::string* const g : greetings)
            delete g;

        greetings.resize(0);
        greetings.reserve(other.greetings.size());

        for (std::string* const g : other.greetings)
            greetings.push_back(new std::string(*g));
    }

    return *this;
}

Greeter& Greeter::operator=(Greeter &&other)
{
    if (this != &other)
    {
        for (std::string* const g : greetings)
            delete g;

        greetings = std::move(other.greetings);
    }

    return *this;
}

Greeter::~Greeter()
{
    for (std::string* const g : greetings)
        delete g;
}

void Greeter::addGreet(const std::string &g)
{
    greetings.push_back(new std::string(g));
}

std::string Greeter::sayHello() const
{
    std::string allGreetings;

    for (std::string* const g : greetings)
        allGreetings += *g + "\n";

    // Assuming that there should be no added newline at the very end.
    if (!allGreetings.empty())
        allGreetings.resize(allGreetings.size()-1);

    return allGreetings;
}
