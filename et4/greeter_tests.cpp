#include "greeter.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <utility>

TEST_CASE("Greeter test", "[greeter]")
{
    std::string testString1 = "Hello, World!";
    std::string testString = testString1;
    Greeter test(testString1);
    CHECK(test.sayHello() == testString);

    std::string testString2 = "Another greeting.";
    testString += "\n" + testString2;
    test.addGreet(testString2);
    CHECK(test.sayHello() == testString);

    std::string testString3;
    testString += "\n" + testString3;
    test.addGreet(testString3);
    CHECK(test.sayHello() == testString);

    std::string testString4 = "Greet4";
    testString += "\n" + testString4;
    test.addGreet(testString4);
    CHECK(test.sayHello() == testString);

    // Test copy constructor
    Greeter b(test);
    CHECK(b.sayHello() == testString);

    // Test copy assignment
    Greeter c("abcdef");
    c = test;
    CHECK(c.sayHello() == testString);

    // Test move constructor
    Greeter d(std::move(test));
    CHECK(d.sayHello() == testString);

    // Test move assignment
    Greeter e("abcdef");
    e = std::move(b);
    CHECK(e.sayHello() == testString);
}
