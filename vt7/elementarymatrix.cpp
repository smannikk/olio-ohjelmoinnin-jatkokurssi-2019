#include "elementarymatrix.hpp"
#include "compositeelement.hpp"
#include "constants.hpp"

#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <utility>

/*!
    @file elementarymatrix.cpp
    @brief Implementation for ElementarySquareMatrix<T>.
*/

static size_t constructFromString(std::vector<std::vector<std::unique_ptr<IntElement>>> &v, const std::string &s);
static size_t constructFromString(std::vector<std::vector<std::unique_ptr<Element>>> &v, const std::string &s);

template <typename T>
ElementarySquareMatrix<T>::ElementarySquareMatrix() : n(0) {}

template <typename T>
ElementarySquareMatrix<T>::ElementarySquareMatrix(const std::string &s)
{
    n = constructFromString(elements, s);

    if (n == M_PARSE_FAILURE)
        throw std::invalid_argument(M_ERROR_INVALID_STRING);
}

template <typename T>
ElementarySquareMatrix<T>::~ElementarySquareMatrix() {}

template <typename T>
ElementarySquareMatrix<T>::ElementarySquareMatrix(const ElementarySquareMatrix<T> &m) : n(m.n)
{
    elements.resize(n);
    for (size_t i=0; i<n; ++i)
    {
        elements.reserve(n);
        for (size_t j=0; j<n; ++j)
            elements[i].push_back(std::unique_ptr<T>(static_cast<T*>(m.elements[i][j]->clone())));
    }
}

template <typename T>
ElementarySquareMatrix<T>::ElementarySquareMatrix(ElementarySquareMatrix<T> &&m) : n(m.n), elements(std::move(m.elements)) {}

template <typename T>
ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator=(const ElementarySquareMatrix<T> &m)
{
    if (this != &m)
        *this = std::move(ElementarySquareMatrix<T>(m));

    return *this;
}

template <typename T>
ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator=(ElementarySquareMatrix<T> &&m)
{
    if (this != &m)
    {
        n = m.n;
        elements = std::move(m.elements);
    }

    return *this;
}

template <typename T>
ElementarySquareMatrix<T> ElementarySquareMatrix<T>::transpose() const
{
    ElementarySquareMatrix<T> transposed;

    size_t size = n;

    std::vector<std::vector<std::unique_ptr<T>>> temp;
    temp.resize(size);

    for (size_t i=0; i<size; ++i)
    {
        temp[i].reserve(size);
        for (size_t j=0; j<size; ++j)
            temp[i].push_back(std::unique_ptr<T>(static_cast<T*>(elements[j][i]->clone())));
    }

    transposed.n = n;
    transposed.elements = std::move(temp);

    return transposed;
}

template <typename T>
bool ElementarySquareMatrix<T>::operator==(const ElementarySquareMatrix<T> &m) const
{
    if (n != m.n)
        return false;

    for (size_t i=0; i<n; ++i)
        for (size_t j=0; j<n; ++j)
            if (*elements[i][j] != *m.elements[i][j])
                return false;

    return true;
}

template <typename T>
void ElementarySquareMatrix<T>::print(std::ostream &os) const
{
    os << M_LEFT_BRACKET;

    for (const auto &v : elements)
    {
        os << M_LEFT_BRACKET;

        bool first = true;
        for (const auto &i : v)
        {
            if (first)
                first = false;
            else
                os << M_DELIM;

            os << *i;
        }

        os << M_RIGHT_BRACKET;
    }

    os << M_RIGHT_BRACKET;
}

template <typename T>
std::string ElementarySquareMatrix<T>::toString() const
{
    std::stringstream ss;
    print(ss);
    return ss.str();
}

template <typename T>
ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator+=(const ElementarySquareMatrix<T> &m)
{
    // Let's assume that the value of n always reflects reality.
    // That is, that the actual size of the vectors never deviates
    // from the size stored in n.
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    auto it1 = elements.begin();
    for (auto it2=m.elements.begin(); it1 != elements.end(); ++it1, ++it2)
        std::transform(it1->begin(), it1->end(), it2->begin(), it1->begin(),
                       [](const std::unique_ptr<T> &e1, const std::unique_ptr<T> &e2)
                       {
                           return std::unique_ptr<T>(static_cast<T*>((*e1 + *e2).clone()));
                       });

    return *this;
}

template <typename T>
ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator-=(const ElementarySquareMatrix<T> &m)
{
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    auto it1 = elements.begin();
    for (auto it2=m.elements.begin(); it1 != elements.end(); ++it1, ++it2)
        std::transform(it1->begin(), it1->end(), it2->begin(), it1->begin(),
                       [](const std::unique_ptr<T> &e1, const std::unique_ptr<T> &e2)
                       {
                           return std::unique_ptr<T>(static_cast<T*>((*e1 - *e2).clone()));
                       });

    return *this;
}

static Element* dotProduct(const ElementarySquareMatrix<Element> &m1, const ElementarySquareMatrix<Element> &m2, size_t row, size_t col)
{
    // Assuming that n >= 1 always. If n == 0, this function is never called.
    CompositeElement elem = *m1.elements[row][0] * *m2.elements[0][col];

    for (size_t i=1; i<m1.n; ++i)
        elem = elem + (*m1.elements[row][i] * *m2.elements[i][col]);

    return elem.clone();
}

static Element* dotProduct(const ElementarySquareMatrix<IntElement> &m1, const ElementarySquareMatrix<IntElement> &m2, size_t row, size_t col)
{
    // Assuming that n >= 1 always. If n == 0, this function is never called.
    IntElement elem = *m1.elements[row][0] * *m2.elements[0][col];

    for (size_t i=1; i<m1.n; ++i)
        elem = elem + (*m1.elements[row][i] * *m2.elements[i][col]);

    return elem.clone();

}

template <typename T>
ElementarySquareMatrix<T>& ElementarySquareMatrix<T>::operator*=(const ElementarySquareMatrix<T> &m)
{
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    /*auto dotProduct = [this, &m](size_t row, size_t col)
    {
        // Assuming that n >= 1 always. If n == 0, this lambda function is never called.
        CompositeElement elem = *elements[row][0] * *m.elements[0][col];

        for (size_t i=1; i<n; ++i)
            elem = elem + (*elements[row][i] * *m.elements[i][col]);

        return elem;
    };*/

    ElementarySquareMatrix<T> temp(*this);

    for (size_t i=0; i<n; ++i)
        for (size_t j=0; j<n; ++j)
            temp.elements[i][j] = std::unique_ptr<T>(static_cast<T*>(dotProduct(*this, m, i, j)));

    *this = std::move(temp);

    return *this;
}

template <typename T>
ElementarySquareMatrix<IntElement> ElementarySquareMatrix<T>::evaluate(const Valuation &val) const
{
    ElementarySquareMatrix<IntElement> temp;

    temp.n = n;
    temp.elements.resize(n);
    for (size_t i=0; i<n; ++i)
    {
        temp.elements[i].reserve(n);
        for (size_t j=0; j<n; ++j)
            temp.elements[i].push_back(std::unique_ptr<IntElement>(new IntElement(elements[i][j]->evaluate(val))));
    }

    return temp;
}

// Only these types can be instantiated. If a truly generic ElementarySquareMatrix
// is needed, the implementation of the member functions above can be moved to the
// header file to achieve that. However, for a type ElementarySquareMatrix<X> to be
// instantiable, a constructFromString(std::vector<std::vector<std::unique_ptr<X>>>,
// const std::string&) function needs to be provided. (Also a dotProduct function is
// needed.)
template class ElementarySquareMatrix<IntElement>;
template class ElementarySquareMatrix<Element>;

static size_t constructFromString(std::vector<std::vector<std::unique_ptr<Element>>> &v, const std::string &s)
{
    std::stringstream ss;
    ss << s;

    char ch;
    ch = ss.get();
    if (ch != M_LEFT_BRACKET)
        return M_PARSE_FAILURE;

    size_t sizeN = 0;
    bool sizeFound = false;
    size_t rows = 0;

    for (; ;++rows)
    {
        std::vector<std::unique_ptr<Element>> row;

        // Once the size is known, allocate enough
        // memory to begin with.
        if (sizeFound)
            row.reserve(sizeN);

        ch = ss.get();
        if (ch != M_LEFT_BRACKET)
        {
            // If no new row is started, the expected
            // character is the final closing bracket.
            if (ch == M_RIGHT_BRACKET)
                break;

            return M_PARSE_FAILURE;
        }

        if (sizeFound && rows>=sizeN)
            return M_PARSE_FAILURE;

        for (size_t n=0; ;)
        {
            // Disallow spaces, tabs, and newlines.
            ch = ss.peek();
            if (ch == ' ' || ch == '\t' || ch == '\n')
                return M_PARSE_FAILURE;

            // Assuming that only letters a-z and A-Z are valid variable names.
            if (isalpha(ch))
            {
                ch = ss.get();
                row.push_back(std::unique_ptr<Element>(new VariableElement(ch)));
            }
            else
            {
                int value;
                ss >> value;
                if (!ss.good())
                    return M_PARSE_FAILURE;

                row.push_back(std::unique_ptr<Element>(new IntElement(value)));
            }

            ++n;

            ch = ss.get();
            if (ch != M_DELIM)
            {
                if (ch == M_RIGHT_BRACKET)
                {
                    v.push_back(std::move(row));

                    if (!sizeFound)
                    {
                        sizeFound = true;
                        sizeN = n;
                        v.reserve(sizeN);
                        break;
                    }
                    else if (n == sizeN)
                        break;

                    return M_PARSE_FAILURE;
                }

                return M_PARSE_FAILURE;
            }
        }
    }

    ch = ss.get();
    if (ch != EOF)
        return M_PARSE_FAILURE;

    // Disallow empty matrices.
    if (!sizeFound)
        return M_PARSE_FAILURE;

    if (rows != sizeN)
        return M_PARSE_FAILURE;

    return sizeN;
}

static size_t constructFromString(std::vector<std::vector<std::unique_ptr<IntElement>>> &v, const std::string &s)
{
    std::stringstream ss;
    ss << s;

    char ch;
    ch = ss.get();
    if (ch != M_LEFT_BRACKET)
        return M_PARSE_FAILURE;

    size_t sizeN = 0;
    bool sizeFound = false;
    size_t rows = 0;

    for (; ;++rows)
    {
        std::vector<std::unique_ptr<IntElement>> row;

        // Once the size is known, allocate enough
        // memory to begin with.
        if (sizeFound)
            row.reserve(sizeN);

        ch = ss.get();
        if (ch != M_LEFT_BRACKET)
        {
            // If no new row is started, the expected
            // character is the final closing bracket.
            if (ch == M_RIGHT_BRACKET)
                break;

            return M_PARSE_FAILURE;
        }

        if (sizeFound && rows>=sizeN)
            return M_PARSE_FAILURE;

        for (size_t n=0; ;)
        {
            // Disallow spaces, tabs, and newlines.
            ch = ss.peek();
            if (ch == ' ' || ch == '\t' || ch == '\n')
                return M_PARSE_FAILURE;

            int value;
            ss >> value;
            if (!ss.good())
                return M_PARSE_FAILURE;

            row.push_back(std::unique_ptr<IntElement>(new IntElement(value)));

            ++n;

            ch = ss.get();
            if (ch != M_DELIM)
            {
                if (ch == M_RIGHT_BRACKET)
                {
                    v.push_back(std::move(row));

                    if (!sizeFound)
                    {
                        sizeFound = true;
                        sizeN = n;
                        v.reserve(sizeN);
                        break;
                    }
                    else if (n == sizeN)
                        break;

                    return M_PARSE_FAILURE;
                }

                return M_PARSE_FAILURE;
            }
        }
    }

    ch = ss.get();
    if (ch != EOF)
        return M_PARSE_FAILURE;

    // Disallow empty matrices.
    if (!sizeFound)
        return M_PARSE_FAILURE;

    if (rows != sizeN)
        return M_PARSE_FAILURE;

    return sizeN;
}
