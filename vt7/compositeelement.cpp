#include "compositeelement.hpp"

#include <utility>

static const char OP_ADD = '+';
static const char OP_SUBTRACT = '-';
static const char OP_MULTIPLY = '*';

CompositeElement::CompositeElement(const Element &e1, const Element &e2, const std::function<int(int, int)> &op, char opc)
:
    oprnd1(std::unique_ptr<Element>(e1.clone())),
    oprnd2(std::unique_ptr<Element>(e2.clone())),
    op_fun(op),
    op_ch(opc)
{}

CompositeElement::CompositeElement(const CompositeElement &e)
:
    oprnd1(std::unique_ptr<Element>(e.oprnd1->clone())),
    oprnd2(std::unique_ptr<Element>(e.oprnd2->clone())),
    op_fun(e.op_fun),
    op_ch(e.op_ch)
{}

CompositeElement::CompositeElement(CompositeElement &&e)
:
    oprnd1(std::move(e.oprnd1)),
    oprnd2(std::move(e.oprnd2)),
    op_fun(std::move(e.op_fun)),
    op_ch(e.op_ch)
{}

CompositeElement& CompositeElement::operator=(const CompositeElement &e)
{
    if (this != &e)
    {
        op_ch = e.op_ch;
        op_fun = e.op_fun;

        oprnd1 = std::unique_ptr<Element>(e.oprnd1->clone());
        oprnd2 = std::unique_ptr<Element>(e.oprnd2->clone());
    }

    return *this;
}

CompositeElement& CompositeElement::operator=(CompositeElement &&e)
{
    if (this != &e)
    {
        op_ch = e.op_ch;
        op_fun = std::move(e.op_fun);

        oprnd1 = std::move(e.oprnd1);
        oprnd2 = std::move(e.oprnd2);
    }

    return *this;
}

CompositeElement::~CompositeElement() {}

int CompositeElement::evaluate(const Valuation &val) const
{
    return op_fun(oprnd1->evaluate(val), oprnd2->evaluate(val));
}

Element* CompositeElement::clone() const
{
    return new CompositeElement(*this);
}

std::string CompositeElement::toString() const
{
    return std::string("(") + oprnd1->toString() + op_ch + oprnd2->toString() + std::string(")");
}

CompositeElement composeAdd(const Element &e1, const Element &e2)
{
    return CompositeElement(e1, e2, [](int i1, int i2){return i1 + i2;}, OP_ADD);
}

CompositeElement composeSubtract(const Element &e1, const Element &e2)
{
    return CompositeElement(e1, e2, [](int i1, int i2){return i1 - i2;}, OP_SUBTRACT);
}

CompositeElement composeMultiply(const Element &e1, const Element &e2)
{
    return CompositeElement(e1, e2, [](int i1, int i2){return i1 * i2;}, OP_MULTIPLY);
}

CompositeElement operator+(const Element &i1, const Element &i2)
{
    return composeAdd(i1, i2);
}

CompositeElement operator-(const Element &i1, const Element &i2)
{
    return composeSubtract(i1, i2);
}

CompositeElement operator*(const Element &i1, const Element &i2)
{
    return composeMultiply(i1, i2);
}
