#ifndef F_ELEMENTARYMATRIX_HPP_INCLUDED
#define F_ELEMENTARYMATRIX_HPP_INCLUDED

#include "element.hpp"

#include <vector>
#include <memory>

/*!
    @file concretematrix.hpp
    @brief Header for the ElementarySquareMatrix template class and functions related to it.

    A printable NxN integer square matrix that supports basic arithmetic operations and one-letter
    symbolic variables.
*/

//! A printable NxN integer square matrix that supports basic arithmetic operations and one-letter
//! symbolic variables.
//! @tparam The (base) type of the elements held in the matrix.
template <typename T>
class ElementarySquareMatrix
{
private:
    size_t n;
    std::vector<std::vector<std::unique_ptr<T>>> elements;
public:
    //! Default constructor.
    ElementarySquareMatrix();

    //! Constructs the matrix from a string given in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @param A string given in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @throw std::invalid_argument if a square matrix cannot be constructed from the string.
    ElementarySquareMatrix(const std::string &s);

    //! Copy constructor
    //! @param Another ElementarySquareMatrix (with corresponding element type)
    ElementarySquareMatrix(const ElementarySquareMatrix<T> &m);

    //! Move constructor
    //! @param Another ElementarySquareMatrix (with corresponding element type)
    ElementarySquareMatrix(ElementarySquareMatrix<T> &&m);

    //! Destructor
    ~ElementarySquareMatrix();

    //! Copy assignment operator
    //! @param Another ElementarySquareMatrix (with corresponding element type)
    //! @return A reference to the matrix that was assigned to
    ElementarySquareMatrix<T>& operator=(const ElementarySquareMatrix<T> &m);

    //! Move assignment operator
    //! @param Another ElementarySquareMatrix (with corresponding element type)
    //! @return A reference to the matrix that was assigned to
    ElementarySquareMatrix<T>& operator=(ElementarySquareMatrix<T> &&m);

    //! @return Returns a matrix that is the transpose of this matrix.
    ElementarySquareMatrix<T> transpose() const;

    //! Element-wise equality comparison of two matrices with corresponding
    //! element type.
    //! @param Another square matrix (with corresponding element type)
    //! @return True, if the elements are all equal. False otherwise.
    bool operator==(const ElementarySquareMatrix<T> &m) const;

    //! Outputs the matrix to the stream in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @param An output stream where the matrix is to be output
    void print(std::ostream& os) const;

    //! Returns the matrix as a string in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @return A string containing a printed version of the matrix
    std::string toString() const;

    //! Constructs and returns a ConcreteSquareMatrix, replacing the symbolic variables with their values according
    //! to the given Valuation map.
    //! @param A map from the variable names to the values
    //! @return A ConcreteSquareMatrix (aka ElementarySquareMatrix<IntElement>)
    //! @throw Throws std::out_of_range exception if no value is mapped for a variable.
    ElementarySquareMatrix<IntElement> evaluate(const Valuation &val) const;

    //! Adds (element-wise) another matrix to the matrix. Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    ElementarySquareMatrix<T>& operator+=(const ElementarySquareMatrix<T> &m);

    //! Subtracts (element-wise) another matrix from the matrix. Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    ElementarySquareMatrix<T>& operator-=(const ElementarySquareMatrix<T> &m);

    //! Multiplies (using matrix-multiplication) the matrix with another matrix.
    //! Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    ElementarySquareMatrix<T>& operator*=(const ElementarySquareMatrix<T> &m);

    template <typename> friend class ElementarySquareMatrix;
    friend Element* dotProduct(const ElementarySquareMatrix<T> &m1, const ElementarySquareMatrix<T> &m2, size_t row, size_t col);
};

//! Outputs the matrix to the stream in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
//! @return Returns a reference to the stream.
//! @param param1 An output stream
//! @param param2 The ElementarySquareMatrix to be output to the stream
template <typename T>
std::ostream& operator<<(std::ostream &os, const ElementarySquareMatrix<T> &m)
{
    m.print(os);
    return os;
}

// Only these types can be instantiated.
using ConcreteSquareMatrix = ElementarySquareMatrix<IntElement>;
using SymbolicSquareMatrix = ElementarySquareMatrix<Element>;

//! Adds (element-wise) two matrices together.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 An ElementarySquareMatrix
//! @param param2 An ElementarySquareMatrix
//! @throw std::invalid_argument if the sizes of the matrices don't match.
template <typename T>
ElementarySquareMatrix<T> operator+(const ElementarySquareMatrix<T> &m1, const ElementarySquareMatrix<T> &m2)
{
    return ElementarySquareMatrix<T>(m1) += m2;
}

//! Subtracts (element-wise) a matrix from another.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 An ElementarySquareMatrix
//! @param param2 An ElementarySquareMatrix
//! @throw std::invalid_argument if the sizes of the matrices don't match.
template <typename T>
ElementarySquareMatrix<T> operator-(const ElementarySquareMatrix<T> &m1, const ElementarySquareMatrix<T> &m2)
{
    return ElementarySquareMatrix<T>(m1) -= m2;
}

//! Multiplies (using matrix-multiplication) two matrices.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 An ElementarySquareMatrix
//! @param param2 An ElementarySquareMatrix
//! @throw std::invalid_argument if the sizes of the matrices don't match.
template <typename T>
ElementarySquareMatrix<T> operator*(const ElementarySquareMatrix<T> &m1, const ElementarySquareMatrix<T> &m2)
{
    return ElementarySquareMatrix<T>(m1) *= m2;
}

#endif // F_ELEMENTARYMATRIX_HPP_INCLUDED
