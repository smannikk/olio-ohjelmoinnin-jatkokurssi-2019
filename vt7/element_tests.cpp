#include "element.hpp"
#include "compositeelement.hpp"

#include "catch.hpp"

#include <sstream>
#include <stdexcept>

TEST_CASE("IntElement misc", "[IntElement]")
{
    CHECK(IntElement(5) == IntElement(5));
    CHECK_FALSE(IntElement(3) == IntElement(5));
    CHECK(IntElement(3) != IntElement(5));
    CHECK_FALSE(IntElement(5) != IntElement(5));

    IntElement a;
    //CHECK(a.getVal() == 0); // No default value for the generic version.

    a.setVal(4);
    CHECK(a.getVal() == 4);
    CHECK(a == IntElement(4));

    IntElement b(a);
    CHECK(a == b);

    IntElement c(0);
    std::stringstream ss;
    ss << c;
    CHECK(ss.str() == "0");

    IntElement d(5);
    IntElement *e = static_cast<IntElement*>(d.clone());
    CHECK(*e == d);
    delete e;

    CHECK(IntElement(5) != IntElement(-5));
}

TEST_CASE("VariableElement misc", "[VariableElement]")
{
    CHECK(VariableElement('x') == VariableElement('x'));
    CHECK_FALSE(VariableElement('y') == VariableElement('x'));
    CHECK(VariableElement('y') != VariableElement('x'));
    CHECK_FALSE(VariableElement('x') != VariableElement('x'));

    VariableElement f;
    //CHECK(f.getVal() == '\0'); // No default value for the generic version.

    f.setVal('x');
    CHECK(f.getVal() == 'x');
    CHECK(f == VariableElement('x'));

    VariableElement g(f);
    CHECK(f == g);

    VariableElement h('z');
    VariableElement *i = static_cast<VariableElement*>(h.clone());
    CHECK(*i == h);
    delete i;

    CHECK(VariableElement('x') != VariableElement('y'));

    Valuation val;
    val['x'] = 1;
    val['y'] = 0;

    CHECK(f.evaluate(val) == 1);
    CHECK(VariableElement('y').evaluate(val) == 0);

    CHECK_THROWS_AS(h.evaluate(val), std::out_of_range);
}

TEST_CASE("CompositeElement misc", "[CompositeElement]")
{
    // Testing only the things that are not covered by SymbolicSquareMatrix' tests.
    // That is, the copy assignment operator and the move constructor.
    CompositeElement a = composeAdd(VariableElement('a'), IntElement(4096));

    CompositeElement b = composeSubtract(VariableElement('y'), IntElement(314159));
    b = a;
    CHECK(b == a);

    CompositeElement c(std::move(b));
    CHECK(c == a);
}

TEST_CASE("IntElement arithmetic", "[IntElement]")
{
    // Testing operator+ suffices, because it is implemented using operator+=
    CHECK(IntElement(4) + IntElement(8) == IntElement(12));
    CHECK(IntElement(9) - IntElement(20) == IntElement(-11));
    CHECK(IntElement(16) * IntElement(16) == IntElement(256));
}
