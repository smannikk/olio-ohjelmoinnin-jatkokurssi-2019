#include "elementarymatrix.hpp"
#include "valuation.hpp"

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include <stack>
#include <iostream>
#include <string>
#include <stdexcept>
#include <sstream>

/*!
    @file main.cpp
    @brief A simple console UI for the square matrix calculator.
*/

static const char *M_STACK_ERROR = "Stack error\n";
static const char *M_SYNTAX_ERROR = "Syntax error\n";
static const char *M_MATH_ERROR = "Math error\n";
static const char *M_VAR_ERROR = "Variable error\n";

int main(int argc, char* argv[])
{
    // Run tests.
    int result = Catch::Session().run(argc, argv);

    if (result != 0)
        return result;

    Valuation variables;
    std::stack<SymbolicSquareMatrix> stack;

    do
    {
        std::string line;
        std::getline(std::cin, line);

        if (line.empty())
            continue;

        if (line[0] == '[')
        {
            try
            {
                stack.push(SymbolicSquareMatrix(line));
            }
            catch (const std::invalid_argument &err)
            {
                //std::cout << "Error: " << err.what() << "\n";
                std::cout << M_SYNTAX_ERROR;
            }
        }
        else if (line == "=")
        {
            if (stack.empty())
            {
                std::cout << M_STACK_ERROR;
                continue;
            }

            try
            {
                std::cout << stack.top().evaluate(variables) << "\n";
            }
            catch (const std::out_of_range &err)
            {
                std::cout << M_VAR_ERROR;
            }
        }
        else if (line == "quit")
        {
            return 0;
        }
        else if (isalpha(line[0]))
        {
            std::stringstream ss(line);
            char var = ss.get();

            if (ss.get() != '=')
            {
                std::cout << M_SYNTAX_ERROR;
                continue;
            }

            int val;
            ss >> val;
            if (ss.fail() || !ss.eof())
            {
                std::cout << M_SYNTAX_ERROR;
                continue;
            }

            variables[var] = val;
        }
        else if (line.size() == 1)
        {
            if (stack.size() < 2)
            {
                std::cout << M_STACK_ERROR;
                continue;
            }

            SymbolicSquareMatrix m2 = std::move(stack.top());
            stack.pop();
            SymbolicSquareMatrix m1 = std::move(stack.top());
            stack.pop();

            try
            {
                switch (line[0])
                {
                    case '+':
                    {
                        stack.push(m1 + m2);
                        break;
                    }
                    case '-':
                    {
                        stack.push(m1 - m2);
                        break;
                    }
                    case '*':
                    {
                        stack.push(m1 * m2);
                        break;
                    }
                    default:
                    {
                        std::cout << M_SYNTAX_ERROR;
                        continue;
                    }
                }
            }
            catch (const std::invalid_argument &err)
            {
                std::cout << M_MATH_ERROR;
                continue;
            }

            std::cout << stack.top() << "\n";
        }
        else
        {
            std::cout << M_SYNTAX_ERROR;
        }
    } while (true);
}
