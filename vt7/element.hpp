#ifndef F_ELEMENT_HPP_INCLUDED
#define F_ELEMENT_HPP_INCLUDED

#include <string>
#include <ostream>

#include "valuation.hpp"

/*!
    @file element.hpp
    @brief Header for the Element class and functions related to it.
*/

//! An abstract base class for an element held in a matrix.
class Element
{
public:
    //! Virtual destructor.
    virtual ~Element();

    //! Pure virtual cloning function.
    virtual Element* clone() const = 0;

    //! Pure virtual string convertion function.
    virtual std::string toString() const = 0;

    //! Pure virtual function for evaluating the value held in an element as an integer.
    virtual int evaluate(const Valuation &val) const = 0;
};

//! Prints the value of an object whose class is derived from Element into the stream.
//! Returns a reference to the stream.
//! @param param1 An output stream.
//! @param param2 An object whose type is derived from Element to be output to the stream.
//! @return A reference to the output stream.
std::ostream& operator<<(std::ostream &os, const Element &i);

//! Comparison operator for two elements. Since the VariableElements cannot be evaluated in
//! this context, the elements are considered equal when the corresponding symbols for both
//! variables and operations are the same.
//! @param param1 An object whose type is derived from Element.
//! @param param2 An object whose type is derived from Element.
//! @return Returns true if the elements are equal. False otherwise.
bool operator==(const Element &e1, const Element &e2);

//! Not-equal-to comparison operator for two elements. Since the VariableElements cannot be
//! evaluated in this context, the elements are considered equal when the corresponding
//! symbols for both variables and operations are the same.
//! @param param1 An object whose type is derived from Element.
//! @param param2 An object whose type is derived from Element.
//! @return Returns true if the elements are not equal. False otherwise.
bool operator!=(const Element &e1, const Element &e2);

class CompositeElement;

//! A (more or less) generic element type.
//! @tparam The type held in the element.
template <typename T>
class TElement : public Element
{
private:
    T value;
public:
    //! Default constructor.
    TElement();

    //! Construct the element from a value.
    //! @param A value of appropriate type.
    TElement(T v);

    //! Destructor.
    ~TElement();

    //! @return Returns the value held in the element.
    T getVal() const;

    //! Set a value to be held in the element.
    //! @param A value of appropriate type.
    void setVal(T val);

    //! Create a dynamically allocated copy of the instance.
    //! @return A pointer of type Element* to a dynamically allocated TElement<T> object.
    Element* clone() const;

    //! @return A string representation of the value.
    std::string toString() const;

    //! @param A mapping from variable names to values.
    //! @return The integer value the element represents or holds.
    //! @throw Throws std::out_of_range exception if no value is mapped for a variable.
    int evaluate(const Valuation &val) const;

    // These arithmetic operations don't make any sense for VariableElements, and they
    // aren't really needed for the IntElements either, but the build server expects
    // them to be declared and defined for IntElement, and by extension for TElement<T>.
    //! Adds to the value another TElement's value. Modifies the original element.
    //! @param param1 The element to be modified
    //! @param param2 The element to be added
    //! @return A reference to the left operand
    TElement<T>& operator+=(const TElement<T> &i);

    //! Subtracts from the value another TElement's value. Modifies the original element.
    //! @param param1 The element to be modified
    //! @param param2 The element to be subtracted
    //! @return A reference to the left operand
    TElement<T>& operator-=(const TElement<T> &i);

    //! Multiplies the value with another TElement's value. Modifies the original element.
    //! @param param1 The element to be modified
    //! @param param2 The element to be multiplied with
    //! @return A reference to the left operand
    TElement<T>& operator*=(const TElement<T> &i);

    //! Converts the TElement<T> to a CompositeElement by adding TElement<int>(0) to the
    //! element.
    operator CompositeElement() const;
};

// Only these types can be instantiated.
using IntElement = TElement<int>;
using VariableElement = TElement<char>;

//! Adds two IntElements.
//! No side effects (the operands are not modified).
//! @param param1 An IntElement
//! @param param2 An IntElement
//! @result The resulting IntElement
IntElement operator+(const IntElement &i1, const IntElement &i2);

//! Subtracts an IntElement from another.
//! No side effects (the operands are not modified).
//! @param param1 An IntElement
//! @param param2 An IntElement
//! @result The resulting IntElement
IntElement operator-(const IntElement &i1, const IntElement &i2);

//! Multiplies two IntElements.
//! No side effects (the operands are not modified).
//! @param param1 An IntElement
//! @param param2 An IntElement
//! @result The resulting IntElement
IntElement operator*(const IntElement &i1, const IntElement &i2);

#endif // F_ELEMENT_HPP_INCLUDED
