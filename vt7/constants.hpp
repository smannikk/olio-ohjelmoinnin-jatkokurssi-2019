#ifndef F_CONSTANTS_HPP_INCLUDED
#define F_CONSTANTS_HPP_INCLUDE

/*!
    @file constants.cpp
    @brief Some constants used by the implementation of both the concrete and the symbolic square matrix.
*/

static const char M_LEFT_BRACKET = '[';
static const char M_RIGHT_BRACKET = ']';
static const char M_DELIM = ',';

static const size_t M_PARSE_FAILURE = 0;

static const char *M_ERROR_SIZE_MISMATCH = "SquareMatrix size mismatch.";
static const char *M_ERROR_INVALID_STRING = "Invalid string argument to SquareMatrix constructor.";

#endif // F_CONSTANTS_HPP_INCLUDED
