#ifndef F_COMPOSITEELEMENT_HPP_INCLUDED
#define F_COMPOSITEELEMENT_HPP_INCLUDED

#include "element.hpp"

#include <functional>
#include <memory>

/*!
    @file compositeelement.hpp
    @brief Header for the CompositeElement class and functions related to it.

    A matrix element that holds two other elements and an operation to be
    performed on them.
*/


//! A class for a matrix element that holds two other elements and an operation
//! to be performed on them.
class CompositeElement : public Element
{
private:
    std::unique_ptr<Element> oprnd1;
    std::unique_ptr<Element> oprnd2;
    std::function<int(int, int)> op_fun;
    char op_ch;
public:
    //! Constructs the element from the given arguments.
    //! @param param1 The element that will be used as the left operand.
    //! @param param2 The element that will be used as the right operand.
    //! @param param3 A function that will perform the operation.
    //! @param param4 A single-letter symbol for the operator.
    CompositeElement(const Element &e1, const Element &e2, const std::function<int(int, int)> &op, char opc);

    //! Copy constructor
    //! @param A CompositeElement to be copied.
    CompositeElement(const CompositeElement &e);

    //! Move constructor
    //! @param A CompositeElement to be moved.
    CompositeElement(CompositeElement &&e);

    //! Copy assignment operator
    //! @param A CompositeElement to be copied.
    //! @return A reference to the object that was assigned to.
    CompositeElement& operator=(const CompositeElement &e);

    //! Move assignment operator
    //! @param A CompositeElement to be moved.
    //! @return A reference to the object that was assigned to.
    CompositeElement& operator=(CompositeElement &&e);

    //! Destructor
    ~CompositeElement();

    //! Create a dynamically allocated copy of the instance.
    //! @return A pointer to a dynamically allocated CompositeElement object.
    Element* clone() const;

    //! @return Returns a string representing the operation (including the operands and the operator).
    std::string toString() const;

    //! @param A map from variable names to integer values.
    //! @return Returns an integer that is the result of the operation on the two elements.
    //! @throw Throws std::out_of_range exception if no value is mapped for a variable.
    int evaluate(const Valuation &val) const;
};

//! Creates a CompositeElement from two elements and defines an addition operation between them.
//! @param param1 Left operand. An object of any class derived from Element.
//! @param param1 Right operand. An object of any class derived from Element.
//! @return A CompositeElement that holds an expression of the from (a+b)
CompositeElement composeAdd(const Element &e1, const Element &e2);

//! Creates a CompositeElement from two elements and defines a subtraction operation between them.
//! @param param1 Left operand. An object of any class derived from Element.
//! @param param1 Right operand. An object of any class derived from Element.
//! @return A CompositeElement that holds an expression of the from (a-b)
CompositeElement composeSubtract(const Element &e1, const Element &e2);

//! Creates a CompositeElement from two elements and defines a multiplication operation between them.
//! @param param1 Left operand. An object of any class derived from Element.
//! @param param1 Right operand. An object of any class derived from Element.
//! @return A CompositeElement that holds an expression of the from (a*b)
CompositeElement composeMultiply(const Element &e1, const Element &e2);


CompositeElement operator+(const Element &i1, const Element &i2);

CompositeElement operator-(const Element &i1, const Element &i2);

CompositeElement operator*(const Element &i1, const Element &i2);

#endif // F_COMPOSITEELEMENT_HPP_INCLUDED
