#include "element.hpp"
#include "compositeelement.hpp"

#include <sstream>

/*!
    @file element.cpp
    @brief Implementation for some of the parts of the abstract Element and for TElement.
*/

Element::~Element() {}

std::ostream& operator<<(std::ostream &os, const Element &i)
{
    return (os << i.toString());
}

bool operator==(const Element &e1, const Element &e2)
{
    // Since we cannot evaluate the symbolic variables (and by extension
    // the expressions), the comparison is done symbolically. This naive
    // symbolic comparison doesn't account for syntactically different
    // expressions that are equivalent, such as situations like (a+1)
    // vs. (1+a) where the operation is commutative. (Or even situations
    // like (1+3) vs (2+2), where the values are known).
    return e1.toString() == e2.toString();
}

bool operator!=(const Element &e1, const Element &e2)
{
    return !(e1 == e2);
}

template <typename T>
TElement<T>::TElement() : value(0) {}

template <typename T>
TElement<T>::TElement(T v) : value(v) {}

template <typename T>
TElement<T>::~TElement() {}

template <typename T>
T TElement<T>::getVal() const
{
    return value;
}

template <typename T>
void TElement<T>::setVal(T val)
{
    value = val;
}

static int eval(char value, const Valuation &val)
{
    return val.at(value);
}

static int eval(int value, const Valuation &val)
{
    return value;
}

template <typename T>
int TElement<T>::evaluate(const Valuation &val) const
{
    // Each type needs a separate eval function, because
    // the evaluation happens differently for them.
    return eval(value, val);
}

template <typename T>
Element* TElement<T>::clone() const
{
    return new TElement<T>(*this);
}

template <typename T>
std::string TElement<T>::toString() const
{
    std::stringstream ss;
    ss << value;
    return ss.str();
}

template <typename T>
TElement<T>& TElement<T>::operator+=(const TElement<T> &i)
{
    value += i.value;

    return *this;
}

template <typename T>
TElement<T>& TElement<T>::operator-=(const TElement<T> &i)
{
    value -= i.value;

    return *this;
}

template <typename T>
TElement<T>& TElement<T>::operator*=(const TElement<T> &i)
{
    value *= i.value;

    return *this;
}

template <typename T>
TElement<T>::operator CompositeElement() const
{
    return composeAdd(*this, IntElement(0));
}

// Only these types can be instantiated. If a truly generic ElementarySquareMatrix
// is needed, the implementation of the member functions above can be moved to the
// header file to achieve that. However, an eval function is still needed for each
// type X before a TElement<X> can be instantiated.
template class TElement<int>;
template class TElement<char>;

IntElement operator+(const IntElement &i1, const IntElement &i2)
{
    return IntElement(i1) += i2;
}

IntElement operator-(const IntElement &i1, const IntElement &i2)
{
    return IntElement(i1) -= i2;
}

IntElement operator*(const IntElement &i1, const IntElement &i2)
{
    return IntElement(i1) *= i2;
}

