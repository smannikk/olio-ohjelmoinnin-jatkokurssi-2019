#include "element.hpp"

/*!
    @file element.cpp
    @brief Implementation for some of the parts of the abstract Element.
*/

Element::~Element() {}

std::ostream& operator<<(std::ostream &os, const Element &i)
{
    return (os << i.toString());
}

bool operator==(const Element &e1, const Element &e2)
{
    // Since we cannot evaluate the symbolic variables (and by extension
    // the expressions), the comparison is done symbolically. This naive
    // symbolic comparison doesn't account for syntactically different
    // expressions that are equivalent, such as situations like (a+1)
    // vs. (1+a) where the operation is commutative. (Or even situations
    // like (1+3) vs (2+2), where the values are known).
    return e1.toString() == e2.toString();
}

bool operator!=(const Element &e1, const Element &e2)
{
    return !(e1 == e2);
}
