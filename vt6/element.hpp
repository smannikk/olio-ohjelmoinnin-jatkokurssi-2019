#ifndef F_ELEMENT_HPP_INCLUDED
#define F_ELEMENT_HPP_INCLUDED

#include <string>
#include <ostream>

#include "valuation.hpp"

/*!
    @file element.hpp
    @brief Header for the Element class and functions related to it.
*/

//! An abstract base class for an element held in a matrix.
class Element
{
public:
    //! Virtual destructor.
    virtual ~Element();

    //! Pure virtual cloning function.
    virtual Element* clone() const = 0;

    //! Pure virtual string convertion function.
    virtual std::string toString() const = 0;

    //! Pure virtual function for evaluating the value held in an element as an integer.
    virtual int evaluate(const Valuation &val) const = 0;
};

//! Prints the value of an object whose class is derived from Element into the stream.
//! Returns a reference to the stream.
//! @param param1 An output stream.
//! @param param2 An object whose type is derived from Element to be output to the stream.
//! @return A reference to the output stream.
std::ostream& operator<<(std::ostream &os, const Element &i);

//! Comparison operator for two elements. Since the VariableElements cannot be evaluated in
//! this context, the elements are considered equal when the corresponding symbols for both
//! variables and operations are the same.
//! @param param1 An object whose type is derived from Element.
//! @param param2 An object whose type is derived from Element.
//! @return Returns true if the elements are equal. False otherwise.
bool operator==(const Element &e1, const Element &e2);

//! Not-equal-to comparison operator for two elements. Since the VariableElements cannot be
//! evaluated in this context, the elements are considered equal when the corresponding
//! symbols for both variables and operations are the same.
//! @param param1 An object whose type is derived from Element.
//! @param param2 An object whose type is derived from Element.
//! @return Returns true if the elements are not equal. False otherwise.
bool operator!=(const Element &e1, const Element &e2);

#endif // F_ELEMENT_HPP_INCLUDED
