#include "symbolicmatrix.hpp"

#include "constants.hpp"
#include "compositeelement.hpp"

#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <cctype>
#include <utility>

/*!
    @file symbolicmatrix.cpp
    @brief SymbolicSquareMatrix implementation.
*/

//! For internal use only. Parse a string into a vector of vectors representing a matrix.
//! @param param1 The vector the numbers are to be read to.
//! @param param2 The string they will be read from.
//! @return Modifies the first parameter. Returns the size N of the NxN SymbolicSquareMatrix (0 on failure, a positive number otherwise).
static size_t constructFromString(std::vector<std::vector<std::unique_ptr<Element>>> &v, const std::string &s);

SymbolicSquareMatrix::SymbolicSquareMatrix() : n(0) {}

SymbolicSquareMatrix::SymbolicSquareMatrix(const std::string &s)
{
    n = constructFromString(elements, s);

    if (n == M_PARSE_FAILURE)
        throw std::invalid_argument(M_ERROR_INVALID_STRING);
}

SymbolicSquareMatrix::SymbolicSquareMatrix(const SymbolicSquareMatrix &m) : n(m.n)
{
    size_t size = n;

    elements.resize(size);
    for (size_t i=0; i<size; ++i)
    {
        elements.reserve(size);
        for (size_t j=0; j<size; ++j)
            elements[i].push_back(std::unique_ptr<Element>(m.elements[i][j]->clone()));
    }
}

SymbolicSquareMatrix::SymbolicSquareMatrix(SymbolicSquareMatrix &&m) : n(m.n), elements(std::move(m.elements)) {}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator=(const SymbolicSquareMatrix &m)
{
    if (this != &m)
        *this = std::move(SymbolicSquareMatrix(m));

    return *this;
}

SymbolicSquareMatrix& SymbolicSquareMatrix::operator=(SymbolicSquareMatrix &&m)
{
    if (this != &m)
    {
        n = m.n;
        elements = std::move(m.elements);
    }

    return *this;
}

SymbolicSquareMatrix::~SymbolicSquareMatrix() {}

SymbolicSquareMatrix SymbolicSquareMatrix::transpose() const
{
    SymbolicSquareMatrix transposed;

    size_t size = n;

    std::vector<std::vector<std::unique_ptr<Element>>> temp;
    temp.resize(size);

    for (size_t i=0; i<size; ++i)
    {
        temp[i].reserve(size);
        for (size_t j=0; j<size; ++j)
            temp[i].push_back(std::unique_ptr<Element>(elements[j][i]->clone()));
    }

    transposed.n = n;
    transposed.elements = std::move(temp);

    return transposed;
}

ConcreteSquareMatrix SymbolicSquareMatrix::evaluate(const Valuation &val) const
{
    std::vector<std::vector<std::unique_ptr<IntElement>>> elems;

    size_t size = n;
    elems.resize(size);

    for (size_t i=0; i<size; ++i)
    {
        elems[i].reserve(size);
        for (size_t j=0; j<size; ++j)
            elems[i].push_back(std::unique_ptr<IntElement>(new IntElement(elements[i][j]->evaluate(val))));
    }

    return ConcreteSquareMatrix(n, std::move(elems));
}

bool SymbolicSquareMatrix::operator==(const SymbolicSquareMatrix &m) const
{
    if (n != m.n)
        return false;

    size_t size = n;
    for (size_t i=0; i<size; ++i)
        for (size_t j=0; j<size; ++j)
            if (*elements[i][j] != *m.elements[i][j])
                return false;

    return true;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator+(const SymbolicSquareMatrix &m) const
{
    // Let's assume that the value of n always reflects reality.
    // That is, that the actual size of the vectors never deviates
    // from the size stored in n.
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    SymbolicSquareMatrix temp(*this);

    auto it1 = temp.elements.begin();
    for (auto it2=m.elements.begin(); it1 != temp.elements.end(); ++it1, ++it2)
        std::transform(it1->begin(), it1->end(), it2->begin(), it1->begin(),
                       [](const std::unique_ptr<Element> &e1, const std::unique_ptr<Element> &e2)
                       {
                           return std::unique_ptr<Element>(static_cast<Element*>(composeAdd(*e1, *e2).clone()));
                       });

    return temp;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator-(const SymbolicSquareMatrix &m) const
{
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    SymbolicSquareMatrix temp(*this);

    auto it1 = temp.elements.begin();
    for (auto it2=m.elements.begin(); it1 != temp.elements.end(); ++it1, ++it2)
        std::transform(it1->begin(), it1->end(), it2->begin(), it1->begin(),
                       [](const std::unique_ptr<Element> &e1, const std::unique_ptr<Element> &e2)
                       {
                           return std::unique_ptr<Element>(static_cast<Element*>(composeSubtract(*e1, *e2).clone()));
                       });

    return temp;
}

SymbolicSquareMatrix SymbolicSquareMatrix::operator*(const SymbolicSquareMatrix &m) const
{
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    size_t size = n;

    auto dotProduct = [this, &m, size](size_t row, size_t col)
    {
        // Assuming that size >= 1 always. If size == 0, this lambda function is never called.
        CompositeElement elem = composeMultiply(*elements[row][0], *m.elements[0][col]);

        for (size_t i=1; i<size; ++i)
            elem = composeAdd(elem, composeMultiply(*elements[row][i], *m.elements[i][col]));

        return elem;
    };

    SymbolicSquareMatrix temp(*this);

    for (size_t i=0; i<size; ++i)
        for (size_t j=0; j<size; ++j)
            temp.elements[i][j] = std::unique_ptr<Element>(static_cast<Element*>(dotProduct(i, j).clone()));

    return temp;
}

void SymbolicSquareMatrix::print(std::ostream &os) const
{
    os << M_LEFT_BRACKET;

    for (const auto &v : elements)
    {
        os << M_LEFT_BRACKET;

        bool first = true;
        for (const auto &i : v)
        {
            if (first)
                first = false;
            else
                os << M_DELIM;

            os << *i;
        }

        os << M_RIGHT_BRACKET;
    }

    os << M_RIGHT_BRACKET;
}


std::ostream& operator<<(std::ostream &os, const SymbolicSquareMatrix &m)
{
    m.print(os);
    return os;
}

std::string SymbolicSquareMatrix::toString() const
{
    std::stringstream ss;
    print(ss);
    return ss.str();
}

static size_t constructFromString(std::vector<std::vector<std::unique_ptr<Element>>> &v, const std::string &s)
{
    std::stringstream ss;
    ss << s;

    char ch;
    ch = ss.get();
    if (ch != M_LEFT_BRACKET)
        return M_PARSE_FAILURE;

    size_t sizeN = 0;
    bool sizeFound = false;
    size_t rows = 0;

    for (; ;++rows)
    {
        std::vector<std::unique_ptr<Element>> row;

        // Once the size is known, allocate enough
        // memory to begin with.
        if (sizeFound)
            row.reserve(sizeN);

        ch = ss.get();
        if (ch != M_LEFT_BRACKET)
        {
            // If no new row is started, the expected
            // character is the final closing bracket.
            if (ch == M_RIGHT_BRACKET)
                break;

            return M_PARSE_FAILURE;
        }

        if (sizeFound && rows>=sizeN)
            return M_PARSE_FAILURE;

        for (size_t n=0; ;)
        {
            // Disallow spaces, tabs, and newlines.
            ch = ss.peek();
            if (ch == ' ' || ch == '\t' || ch == '\n')
                return M_PARSE_FAILURE;

            // Assuming that only letters a-z and A-Z are valid variable names.
            if (isalpha(ch))
            {
                ch = ss.get();
                row.push_back(std::unique_ptr<Element>(new VariableElement(ch)));
            }
            else
            {
                int value;
                ss >> value;
                if (!ss.good())
                    return M_PARSE_FAILURE;

                row.push_back(std::unique_ptr<Element>(new IntElement(value)));
            }

            ++n;

            ch = ss.get();
            if (ch != M_DELIM)
            {
                if (ch == M_RIGHT_BRACKET)
                {
                    v.push_back(std::move(row));

                    if (!sizeFound)
                    {
                        sizeFound = true;
                        sizeN = n;
                        v.reserve(sizeN);
                        break;
                    }
                    else if (n == sizeN)
                        break;

                    return M_PARSE_FAILURE;
                }

                return M_PARSE_FAILURE;
            }
        }
    }

    ch = ss.get();
    if (ch != EOF)
        return M_PARSE_FAILURE;

    // Disallow empty matrices.
    if (!sizeFound)
        return M_PARSE_FAILURE;

    if (rows != sizeN)
        return M_PARSE_FAILURE;

    return sizeN;
}
