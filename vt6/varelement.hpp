#ifndef F_VARELEMENT_HPP_INCLUDED
#define F_VARELEMENT_HPP_INCLUDED

/*!
    @file varelement.hpp
    @brief Header for the VariableElement class and functions related to it.

    A single letter symbolic variable.
*/

#include "element.hpp"

//! A single letter symbolic variable.
class VariableElement : public Element
{
private:
    char var;
public:
    //! Default constructor. Sets the var to '\0'
    VariableElement();

    //! Constructs the element with the given variable name.
    //! @param The name for the variable.
    VariableElement(char v);

    //! Destructor.
    ~VariableElement();

    //! @return Returns the name of the variable.
    char getVal() const;

    //! Sets the name of the variable.
    //! @param The new name for the variable.
    void setVal(char v);

    //! Create a dynamically allocated copy of the instance.
    //! @return A pointer to a dynamically allocated VariableElement object.
    Element* clone() const;

    //! @return Returns a string containing the variable name
    std::string toString() const;

    //! @param A map from variable names to integer values.
    //! @return Returns an integer value mapped to the variable name.
    //! @throw Throws std::out_of_range exception if no value is mapped for the variable.
    int evaluate(const Valuation &val) const;

    //! Compares two VariableElements by value (the variable name) to see if they are the same variable.
    //! @return true, if they are the same variable, false otherwise
    bool operator==(const VariableElement &i) const;

    //! Compares two VariableElements by value (the variable name) to see if they are not the same variable.
    //! @return true, if they are the same variable, false otherwise
    bool operator!=(const VariableElement &i) const;
};

#endif // F_VARELEMENT_HPP_INCLUDED
