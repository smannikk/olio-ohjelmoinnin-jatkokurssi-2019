#include "concretematrix.hpp"
#include "symbolicmatrix.hpp"
#include "intelement.hpp"
#include "varelement.hpp"
#include "valuation.hpp"
#include "compositeelement.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <sstream>
#include <stdexcept>
#include <utility>

const std::vector<std::string> g_testStrings = {"[[1]]", "[[1,2][4,5]]", "[[-1,2][4,-5]]",
                                                "[[0,0][0,0]]", "[[1,2,3][4,5,6][7,8,9]]",
                                                "[[112323,-43432,33434][434,-434345,0][7,8,9]]",
                                                "[[1,2,3,1,2,3][4,5,6,1,2,3][7,8,9,1,2,3][1,2,3,1,2,3][4,5,6,1,2,3][7,8,9,1,2,3]]"};

TEST_CASE("SquareMatrix IO", "[ConcreteSquareMatrix, SymbolicSquareMatrix]")
{
    for (const std::string &s : g_testStrings)
    {
        ConcreteSquareMatrix a(s);
        SymbolicSquareMatrix b(s);

        CHECK(a.toString() == s);
        CHECK(b.toString() == s);

        std::stringstream ss, ss2;
        ss << a;
        CHECK(ss.str() == s);

        ss2 << b;
        CHECK(ss2.str() == s);
    }

    CHECK(ConcreteSquareMatrix("[[+0,0][0,0]]").toString() == "[[0,0][0,0]]");
    CHECK(SymbolicSquareMatrix("[[+0,0][0,0]]").toString() == "[[0,0][0,0]]");

    CHECK(SymbolicSquareMatrix("[[a,0][0,0]]").toString() == "[[a,0][0,0]]");
    CHECK(SymbolicSquareMatrix("[[a,0][x,y]]").toString() == "[[a,0][x,y]]");

    CHECK((SymbolicSquareMatrix("[[a,0][b,1]]") + SymbolicSquareMatrix("[[c,3][2,4]]")).toString() == "[[(a+c),(0+3)][(b+2),(1+4)]]");
}

TEST_CASE("SquareMatrix arithmetic", "[ConcreteSquareMatrix]")
{
    // Testing operator+ suffices, because it is implemented using operator+=
    CHECK(ConcreteSquareMatrix("[[1,2][3,4]]") + ConcreteSquareMatrix("[[4,3][2,1]]") == ConcreteSquareMatrix("[[5,5][5,5]]"));
    CHECK(ConcreteSquareMatrix("[[2,2][2,2]]") - ConcreteSquareMatrix("[[1,2][3,4]]") == ConcreteSquareMatrix("[[1,0][-1,-2]]"));
    CHECK(ConcreteSquareMatrix("[[1,2][3,4]]") * ConcreteSquareMatrix("[[5,6][7,8]]") == ConcreteSquareMatrix("[[19,22][43,50]]"));

    CHECK(ConcreteSquareMatrix("[[1]]") + ConcreteSquareMatrix("[[2]]") == ConcreteSquareMatrix("[[3]]"));
    CHECK(ConcreteSquareMatrix("[[1]]") - ConcreteSquareMatrix("[[2]]") == ConcreteSquareMatrix("[[-1]]"));
    CHECK(ConcreteSquareMatrix("[[5]]") * ConcreteSquareMatrix("[[2]]") == ConcreteSquareMatrix("[[10]]"));

    CHECK(ConcreteSquareMatrix("[[1,2,3][4,5,6][7,8,9]]") + ConcreteSquareMatrix("[[4,5,7][8,6,9][1,1,2]]") == ConcreteSquareMatrix("[[5,7,10][12,11,15][8,9,11]]"));
    CHECK(ConcreteSquareMatrix("[[1,2,3][4,5,6][7,8,9]]") - ConcreteSquareMatrix("[[4,5,7][8,6,9][1,1,2]]") == ConcreteSquareMatrix("[[-3,-3,-4][-4,-1,-3][6,7,7]]"));
    CHECK(ConcreteSquareMatrix("[[1,2,3][4,5,6][7,8,9]]") * ConcreteSquareMatrix("[[4,5,7][8,6,9][1,1,2]]") == ConcreteSquareMatrix("[[23,20,31][62,56,85][101,92,139]]"));

    #define M_SHOULD_THROW(param1, param2) CHECK_THROWS_AS(ConcreteSquareMatrix(param1) + ConcreteSquareMatrix(param2), std::invalid_argument); \
                                           CHECK_THROWS_AS(ConcreteSquareMatrix(param1) - ConcreteSquareMatrix(param2), std::invalid_argument); \
                                           CHECK_THROWS_AS(ConcreteSquareMatrix(param1) * ConcreteSquareMatrix(param2), std::invalid_argument)

    M_SHOULD_THROW("[[1,2][3,4]]", "[[5]]");
    M_SHOULD_THROW("[[1,2,3][3,4,5][3,4,5]]", "[[5,2][5,3]]");

    #undef M_SHOULD_THROW
}

TEST_CASE("SquareMatrix constructors", "[ConcreteSquareMatrix, SymbolicSquareMatrix]")
{
    for (const std::string &s : g_testStrings)
    {
        CHECK_NOTHROW(ConcreteSquareMatrix(s));
        CHECK_NOTHROW(SymbolicSquareMatrix(s));
    }

    CHECK_NOTHROW(ConcreteSquareMatrix("[[+0,0][0,0]]"));
    CHECK_NOTHROW(SymbolicSquareMatrix("[[+0,0][0,0]]"));

    CHECK_NOTHROW(SymbolicSquareMatrix("[[a,0][0,0]]"));
    CHECK_NOTHROW(SymbolicSquareMatrix("[[a,b][c,d]]"));

    #define M_SHOULD_THROW(x) CHECK_THROWS_AS(ConcreteSquareMatrix(x), std::invalid_argument); \
                              CHECK_THROWS_AS(SymbolicSquareMatrix(x), std::invalid_argument)

    M_SHOULD_THROW("");
    M_SHOULD_THROW("[]");
    M_SHOULD_THROW("[");
    M_SHOULD_THROW("]");
    M_SHOULD_THROW("[[]");
    M_SHOULD_THROW("[]]");
    M_SHOULD_THROW("[[]]");
    M_SHOULD_THROW("[[],[]]");

    M_SHOULD_THROW("[2]");
    M_SHOULD_THROW("[1,2,3][1,2,3][1,2,3]");
    M_SHOULD_THROW("[[1, 2, 3], [1, 2, 3], [1, 2, 3]]");
    M_SHOULD_THROW("[[1,2,3][1,2,3][1,2,3]] ");
    M_SHOULD_THROW(" [[1,2,3][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[1,2,3,]][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[1,2,3][1,2,3][1,2,3]");
    M_SHOULD_THROW("[1,2,3][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[1,2,3][1,2,3][1,2,3]]a");
    M_SHOULD_THROW("b[[1,2,3][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[1,2,3] [4,5,6] [7,8,9]]");
    M_SHOULD_THROW("[[1,2,3],[4,5,6],[7,8,9]]");
    M_SHOULD_THROW("[[0.0,0][0,0]]");
    M_SHOULD_THROW("[[0.2,0][0,0]]");
    M_SHOULD_THROW("[[1.2,0][0,0]]");
    M_SHOULD_THROW("[[--0,0][0,0]]");
    M_SHOULD_THROW("[[+-0,0][0,0]]");
    M_SHOULD_THROW("[[1,2][3,4]][[1,2][3,4]]");
    M_SHOULD_THROW("[[0x1C9,2][3,4]]");
    M_SHOULD_THROW("((1,2)(4,5))");
    M_SHOULD_THROW("[[1 2][4 5]]");
    M_SHOULD_THROW("[[1;2][4;5]]");
    M_SHOULD_THROW("[[1-2][4-5]]");
    M_SHOULD_THROW("[[1,\n2,3][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[aa,2][3,4]]");
    M_SHOULD_THROW("[[a,bc][3,4]]");
    M_SHOULD_THROW("[[;,2][3,4]]");
    M_SHOULD_THROW("[[[,2][3,4]]");

    // Incorrect spacing.
    M_SHOULD_THROW("[[1, 2, 3][1, 2, 3][1, 2, 3]]");

    // Non-square matrices
    M_SHOULD_THROW("[[1,2,3][2,3,4][5,4,2][2,3,4]]");
    M_SHOULD_THROW("[[1,2][3,4,5]]");
    M_SHOULD_THROW("[[1,2]]");

    // Overflow (for reference, 9223372036854775807 is the maximum value for a signed 64-bit integer)
    M_SHOULD_THROW("[[1,2][4,59999999999999999999999999999999999]]");
    M_SHOULD_THROW("[[1,2][4,-59999999999999999999999999999999999]]");

    #undef M_SHOULD_THROW

    CHECK_THROWS_AS(ConcreteSquareMatrix("[[a,2][3,4]]"), std::invalid_argument);
    CHECK_THROWS_AS(ConcreteSquareMatrix("[[a,b][c,d]]"), std::invalid_argument);

    #define M_MISC_TESTS(SquareMatrix) {           \
    /* Copy constructor */                          \
    SquareMatrix a("[[1,2][3,4]]");                  \
    CHECK(SquareMatrix(a) == a);                      \
                                                       \
    /* Copy assignment */                               \
    SquareMatrix b;                                      \
    b = a;                                                \
    CHECK(a == b);                                         \
                                                            \
    SquareMatrix b2("[[1,2,3][4,5,6][7,8,9]]");              \
    b2 = a;                                                   \
    CHECK(b2 == a);                                            \
                                                                \
    /* Move constructor */                                       \
    SquareMatrix c(std::move(a));                                 \
    CHECK(c == b);                                                 \
                                                                    \
    /* Move assignment */                                            \
    SquareMatrix d;                                                   \
    d = std::move(b);                                                  \
    CHECK(c == d);                                                      \
                                                                         \
    SquareMatrix d2("[[1,2,3][4,5,6][7,8,9]]");                           \
    d2 = std::move(d);                                                     \
    CHECK(c == d2);                                                         \
                                                                             \
    /* A negative test case for the comparison operator: */                   \
    CHECK_FALSE(SquareMatrix("[[1,2][3,4]]") == SquareMatrix("[[2,3][4,5]]")); \
    CHECK_FALSE(SquareMatrix("[[1,2][3,4]]") == SquareMatrix("[[1]]"));         \
    }

    M_MISC_TESTS(ConcreteSquareMatrix);
    M_MISC_TESTS(SymbolicSquareMatrix);

    #undef M_MISC_TESTS

    CHECK(SymbolicSquareMatrix("[[a,1][2,3]]") == SymbolicSquareMatrix("[[a,1][2,3]]"));
    CHECK_FALSE(SymbolicSquareMatrix("[[a,1][2,3]]") == SymbolicSquareMatrix("[[b,1][2,3]]"));
    CHECK_FALSE(SymbolicSquareMatrix("[[a,1][2,3]]") == SymbolicSquareMatrix("[[0,1][2,3]]"));
    CHECK_FALSE(SymbolicSquareMatrix("[[a,1][2,3]]") == SymbolicSquareMatrix("[[a,1][2,b]]"));

    SymbolicSquareMatrix a("[[a,1][b,c]]");
    a = a + SymbolicSquareMatrix("[[c,a][1,2]]");

    SymbolicSquareMatrix b(a);
    CHECK(b == a);

    SymbolicSquareMatrix c;
    c = a;
    CHECK(c == a);

    SymbolicSquareMatrix d(std::move(b));
    CHECK(d == a);

    SymbolicSquareMatrix e;
    e = std::move(d);
    CHECK(e == a);

    c = c * e;
    CHECK_FALSE(c == a);
    CHECK(c == a*e);
}

TEST_CASE("SquareMatrix transpose", "[ConcreteSquareMatrix, SymbolicSquareMatrix]")
{
    std::vector<std::pair<std::string, std::string>> test = {{"[[1,2][3,4]]", "[[1,3][2,4]]"},
                                                             {"[[1]]", "[[1]]"},
                                                             {"[[1,2,3][4,5,6][7,8,9]]", "[[1,4,7][2,5,8][3,6,9]]"}};

    for (const auto &p : test)
    {
        CHECK(ConcreteSquareMatrix(p.first).transpose() == ConcreteSquareMatrix(p.second));
        CHECK(SymbolicSquareMatrix(p.first).transpose() == SymbolicSquareMatrix(p.second));
    }

    CHECK(SymbolicSquareMatrix("[[1,a][3,4]]").transpose() == SymbolicSquareMatrix("[[1,3][a,4]]"));
}

TEST_CASE("SymbolicSquareMatrix evaluation and calculations", "[SymbolicSquareMatrix]")
{
    Valuation val;
    val['x'] = 1;
    val['y'] = 0;
    val['z'] = -1;

    SymbolicSquareMatrix a("[[x,1][2,4]]");
    CHECK(a.evaluate(val) == ConcreteSquareMatrix("[[1,1][2,4]]"));

    CHECK_THROWS_AS(SymbolicSquareMatrix("[[f]]").evaluate(val), std::out_of_range);

    CHECK(SymbolicSquareMatrix("[[x,y][x,z]]").evaluate(val).toString() == "[[1,0][1,-1]]");

    SymbolicSquareMatrix b("[[x,y][z,1]]");
    CHECK((a + b).evaluate(val) == a.evaluate(val) + b.evaluate(val));
    CHECK((a - b).evaluate(val) == a.evaluate(val) - b.evaluate(val));
    CHECK((b - a).evaluate(val) == b.evaluate(val) - a.evaluate(val));
    CHECK((a * b).evaluate(val) == a.evaluate(val) * b.evaluate(val));

    CHECK(((a * b) + (a - b)).evaluate(val) == (a.evaluate(val) * b.evaluate(val)) + (a.evaluate(val) - b.evaluate(val)));

    #define M_SHOULD_THROW(param1, param2) CHECK_THROWS_AS(SymbolicSquareMatrix(param1) + SymbolicSquareMatrix(param2), std::invalid_argument); \
                                           CHECK_THROWS_AS(SymbolicSquareMatrix(param1) - SymbolicSquareMatrix(param2), std::invalid_argument); \
                                           CHECK_THROWS_AS(SymbolicSquareMatrix(param1) * SymbolicSquareMatrix(param2), std::invalid_argument)

    M_SHOULD_THROW("[[1,2][3,4]]", "[[5]]");
    M_SHOULD_THROW("[[1,2,3][3,4,5][3,4,5]]", "[[5,2][5,3]]");
    M_SHOULD_THROW("[[1,2,a][3,4,b][3,c,5]]", "[[d,2][5,3]]");
    M_SHOULD_THROW("[[1,2][x,4]]", "[[y]]");

    #undef M_SHOULD_THROW
}

TEST_CASE("IntElement misc", "[IntElement]")
{
    CHECK(IntElement(5) == IntElement(5));
    CHECK_FALSE(IntElement(3) == IntElement(5));
    CHECK(IntElement(3) != IntElement(5));
    CHECK_FALSE(IntElement(5) != IntElement(5));

    IntElement a;
    CHECK(a.getVal() == 0);

    a.setVal(4);
    CHECK(a.getVal() == 4);
    CHECK(a == IntElement(4));

    IntElement b(a);
    CHECK(a == b);

    IntElement c(0);
    std::stringstream ss;
    ss << c;
    CHECK(ss.str() == "0");

    IntElement d(5);
    IntElement *e = static_cast<IntElement*>(d.clone());
    CHECK(*e == d);
    delete e;

    CHECK(IntElement(5) != IntElement(-5));
}

TEST_CASE("VariableElement misc", "[VariableElement]")
{
    CHECK(VariableElement('x') == VariableElement('x'));
    CHECK_FALSE(VariableElement('y') == VariableElement('x'));
    CHECK(VariableElement('y') != VariableElement('x'));
    CHECK_FALSE(VariableElement('x') != VariableElement('x'));

    VariableElement f;
    CHECK(f.getVal() == '\0');

    f.setVal('x');
    CHECK(f.getVal() == 'x');
    CHECK(f == VariableElement('x'));

    VariableElement g(f);
    CHECK(f == g);

    VariableElement h('z');
    VariableElement *i = static_cast<VariableElement*>(h.clone());
    CHECK(*i == h);
    delete i;

    CHECK(VariableElement('x') != VariableElement('y'));

    Valuation val;
    val['x'] = 1;
    val['y'] = 0;

    CHECK(f.evaluate(val) == 1);
    CHECK(VariableElement('y').evaluate(val) == 0);

    CHECK_THROWS_AS(h.evaluate(val), std::out_of_range);
}

TEST_CASE("CompositeElement misc", "[CompositeElement]")
{
    // Testing only the things that are not covered by SymbolicSquareMatrix' tests.
    // That is, the copy assignment operator and the move constructor.
    CompositeElement a = composeAdd(VariableElement('a'), IntElement(4096));

    CompositeElement b = composeSubtract(VariableElement('y'), IntElement(314159));
    b = a;
    CHECK(b == a);

    CompositeElement c(std::move(b));
    CHECK(c == a);
}

TEST_CASE("IntElement arithmetic", "[IntElement]")
{
    // Testing operator+ suffices, because it is implemented using operator+=
    CHECK(IntElement(4) + IntElement(8) == IntElement(12));
    CHECK(IntElement(9) - IntElement(20) == IntElement(-11));
    CHECK(IntElement(16) * IntElement(16) == IntElement(256));
}
