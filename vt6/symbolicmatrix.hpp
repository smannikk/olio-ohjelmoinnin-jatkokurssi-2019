#ifndef F_SYMBOLICMATRIX_HPP_INCLUDED
#define F_SYMBOLICMATRIX_HPP_INCLUDED

#include "concretematrix.hpp"
#include "varelement.hpp"

/*!
    @file concretematrix.hpp
    @brief Header for the SymbolicSquareMatrix class and functions related to it.

    A printable NxN integer square matrix that can contain single-letter symbolic variables.
*/

//! A printable NxN integer square matrix that can contain single-letter symbolic variables.
class SymbolicSquareMatrix
{
private:
    unsigned n;
    std::vector<std::vector<std::unique_ptr<Element>>> elements;
public:
    //! Default constructor.
    //! @return An empty SymbolicSquareMatrix
    SymbolicSquareMatrix();

    //! Constructs the matrix from a string given in the format: [[a11,x,...,a1n][a21,y,...,a2n]...[an1,an2,...,ann]],
    //! where a11, a12, ... are integer values, and x, y are one-letter variable names.
    //! @param A string given in the format: [[a11,x,...,a1n][a21,y,...,a2n]...[an1,an2,...,ann]]
    //! @return The resulting SymbolicSquareMatrix
    //! @throw std::invalid_argument if a SymbolicSquareMatrix cannot be constructed from the string.
    SymbolicSquareMatrix(const std::string &s);

    //! Copy constructor
    //! @param A SymbolicSquareMatrix to be copied
    //! @return A copy of the input matrix
    SymbolicSquareMatrix(const SymbolicSquareMatrix &m);

    //! Move constructor
    //! @param A SymbolicSquareMatrix to be moved
    //! @return A SymbolicSquareMatrix
    SymbolicSquareMatrix(SymbolicSquareMatrix &&m);

    //! Copy assignment operator
    //! @param param1 The matrix to be assigned to
    //! @param param2 The matrix to be copied
    //! @return A reference to the left operand
    SymbolicSquareMatrix& operator=(const SymbolicSquareMatrix &m);

    //! Move assignment operator
    //! @param param1 The matrix to be assigned to
    //! @param param2 The matrix to be moved
    //! @return A reference to the left operand
    SymbolicSquareMatrix& operator=(SymbolicSquareMatrix &&m);

    //! Destructor.
    ~SymbolicSquareMatrix();

    //! @return Returns a matrix that is the transpose of this matrix.
    SymbolicSquareMatrix transpose() const;

    //! Element-wise equality comparison of two matrices.
    //! @param Another SymbolicSquareMatrix
    //! @return True, if the elements are all equal. False otherwise.
    bool operator==(const SymbolicSquareMatrix &m) const;

    //! Outputs the matrix to the stream in the format: [[a11,a12,...,a1n][a21,x,...,a2n]...[an1,y,...,ann]]
    //! @param An output stream where the matrix is to be output
    void print(std::ostream& os) const;

    //! Returns the matrix as a string in the format: [[a11,a12,...,a1n][a21,x,...,a2n]...[an1,y,...,ann]]
    //! @return A string containing a printed version of the matrix
    std::string toString() const;

    //! Constructs and returns a ConcreteSquareMatrix, replacing the symbolic variables with their values according
    //! to the given Valuation map.
    //! @param A map from the variable names to the values.
    //! @return A ConcreteSquareMatrix
    ConcreteSquareMatrix evaluate(const Valuation &val) const;

    //! Adds (element-wise) two matrices.
    //! @param param1 A matrix to be used as the left operand.
    //! @param param2 A matrix to be used as the right operand.
    //! @return A new SymbolicMatrix that is the result of the operation.
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    SymbolicSquareMatrix operator+(const SymbolicSquareMatrix &m) const;

    //! Subtracts (element-wise) two matrices.
    //! @param param1 A matrix to be used as the left operand.
    //! @param param2 A matrix to be used as the right operand.
    //! @return A new SymbolicMatrix that is the result of the operation.
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    SymbolicSquareMatrix operator-(const SymbolicSquareMatrix &m) const;

    //! Multiplies (using matrix-multiplication) two matrices.
    //! Modifies the original matrix.
    //! @param param1 A matrix to be used as the left operand.
    //! @param param2 A matrix to be used as the right operand.
    //! @return A new SymbolicMatrix that is the result of the operation.
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    SymbolicSquareMatrix operator*(const SymbolicSquareMatrix &m) const;
};

//! Outputs the matrix to the stream in the format: [[a11,x,...,a1n][a21,y,...,a2n]...[an1,an2,...,ann]]
//! @return Returns a reference to the stream.
//! @param param1 An output stream
//! @param param2 The SymbolicSquareMatrix to be output to the stream
std::ostream& operator<<(std::ostream &os, const SymbolicSquareMatrix &m);

#endif // F_SYMBOLICMATRIX_HPP_INCLUDED
