#include "issqmatrix.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE("Testing valid square matrix inputs.", "[correct]")
{
    CHECK(isSquareMatrix("[[1]]")); // 1x1 matrix is a square matrix
    CHECK(isSquareMatrix("[[1,2][4,5]]"));
    CHECK(isSquareMatrix("[[-1,2][4,-5]]"));
    CHECK(isSquareMatrix("[[0,0][0,0]]"));
    CHECK(isSquareMatrix("[[1,2,3][4,5,6][7,8,9]]"));
    CHECK(isSquareMatrix("[[112323,-43432,33434][434,-434345,0][7,8,9]]"));
    CHECK(isSquareMatrix("[[1,2,3,1,2,3][4,5,6,1,2,3][7,8,9,1,2,3][1,2,3,1,2,3][4,5,6,1,2,3][7,8,9,1,2,3]]"));

    // How about an explicit positive sign?
    CHECK(isSquareMatrix("[[+0,0][0,0]]"));
}

TEST_CASE("Testing invalid square matrix inputs.", "[incorrect]")
{
    CHECK_FALSE(isSquareMatrix(""));
    CHECK_FALSE(isSquareMatrix("[]"));
    CHECK_FALSE(isSquareMatrix("["));
    CHECK_FALSE(isSquareMatrix("]"));
    CHECK_FALSE(isSquareMatrix("[[]"));
    CHECK_FALSE(isSquareMatrix("[]]"));
    CHECK_FALSE(isSquareMatrix("[[]]"));
    CHECK_FALSE(isSquareMatrix("[[],[]]"));

    CHECK_FALSE(isSquareMatrix("[2]"));
    CHECK_FALSE(isSquareMatrix("[1,2,3][1,2,3][1,2,3]"));
    CHECK_FALSE(isSquareMatrix("[[1, 2, 3], [1, 2, 3], [1, 2, 3]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3][1,2,3][1,2,3]] "));
    CHECK_FALSE(isSquareMatrix(" [[1,2,3][1,2,3][1,2,3]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3,]][1,2,3][1,2,3]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3][1,2,3][1,2,3]"));
    CHECK_FALSE(isSquareMatrix("[1,2,3][1,2,3][1,2,3]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3][1,2,3][1,2,3]]a"));
    CHECK_FALSE(isSquareMatrix("b[[1,2,3][1,2,3][1,2,3]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3] [4,5,6] [7,8,9]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2,3],[4,5,6],[7,8,9]]"));
    CHECK_FALSE(isSquareMatrix("[[0.0,0][0,0]]"));
    CHECK_FALSE(isSquareMatrix("[[0.2,0][0,0]]"));
    CHECK_FALSE(isSquareMatrix("[[1.2,0][0,0]]"));
    CHECK_FALSE(isSquareMatrix("[[--0,0][0,0]]"));
    CHECK_FALSE(isSquareMatrix("[[+-0,0][0,0]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2][3,4]][[1,2][3,4]]"));
    CHECK_FALSE(isSquareMatrix("[[a,2][3,4]]"));
    CHECK_FALSE(isSquareMatrix("[[a,b][c,d]]"));
    CHECK_FALSE(isSquareMatrix("[[0x1C9,2][3,4]]"));
    CHECK_FALSE(isSquareMatrix("((1,2)(4,5))"));
    CHECK_FALSE(isSquareMatrix("[[1 2][4 5]]"));
    CHECK_FALSE(isSquareMatrix("[[1;2][4;5]]"));
    CHECK_FALSE(isSquareMatrix("[[1-2][4-5]]"));
    CHECK_FALSE(isSquareMatrix("[[1,\n2,3][1,2,3][1,2,3]]"));

    // Different spacing. Should this be correct or incorrect?
    // Let's assume it's incorrect.
    CHECK_FALSE(isSquareMatrix("[[1, 2, 3][1, 2, 3][1, 2, 3]]"));

    // Non-square matrices
    CHECK_FALSE(isSquareMatrix("[[1,2,3][2,3,4][5,4,2][2,3,4]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2][3,4,5]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2]]"));

    // Overflow (for reference, 9223372036854775807 is the maximum value for a signed 64-bit integer)
    CHECK_FALSE(isSquareMatrix("[[1,2][4,59999999999999999999999999999999999]]"));
    CHECK_FALSE(isSquareMatrix("[[1,2][4,-59999999999999999999999999999999999]]"));
}
