#include <sstream>

#include "issqmatrix.hpp"

const char M_LEFT_BRACKET = '[';
const char M_RIGHT_BRACKET = ']';
const char M_DELIM = ',';

bool isSquareMatrix(const std::string& str)
{
    std::stringstream ss;
    ss << str;

    char ch;
    ch = ss.get();
    if (ch != M_LEFT_BRACKET)
        return false;

    size_t sizeN = 0;
    bool sizeFound = false;
    size_t rows = 0;

    for (; ;++rows)
    {
        ch = ss.get();
        if (ch != M_LEFT_BRACKET)
        {
            // If no new row is started, the expected
            // character is the final closing bracket.
            if (ch == M_RIGHT_BRACKET)
                break;

            return false;
        }

        if (sizeFound && rows>=sizeN)
            return false;

        for (size_t n=0; ;)
        {
            // Disallow spacing between numbers, as the example
            // doesn't have spaces. Let's assume it is to be
            // strictly followed. Also disallow tabs and newlines.
            ch = ss.peek();
            if (ch == ' ' || ch == '\t' || ch == '\n')
                return false;

            int value;
            ss >> value;
            if (!ss.good())
                return false;

            ++n;

            ch = ss.get();
            if (ch != M_DELIM)
            {
                if (ch == M_RIGHT_BRACKET)
                {
                    if (!sizeFound)
                    {
                        sizeFound = true;
                        sizeN = n;
                        break;
                    }
                    else if (n == sizeN)
                        break;

                    return false;
                }

                return false;
            }
        }
    }

    ch = ss.get();
    if (ch != EOF)
        return false;

    // Disallow empty matrices.
    if (!sizeFound)
        return false;

    if (rows != sizeN)
        return false;

    return true;
}
