#ifndef F_ISSQMATRIX_HPP_INCLUDED
#define F_ISSQMATRIX_HPP_INCLUDED

#include <string>

// Checks whether a given input string can be parsed as a square matrix.
// For example, "[[1,2,3][4,5,6][7,8,9]]" would be a valid square matrix.
bool isSquareMatrix(const std::string& str);

#endif // F_ISSQMATRIX_HPP_INCLUDED
