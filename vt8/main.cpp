#define M_CATCH_TESTS_ONLY 1

#if M_CATCH_TESTS_ONLY

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#else

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "squarematrix.hpp"

void test(size_t n);

int main(int argc, char* argv[])
{
    #if 0
    // Run tests.
    int result = Catch::Session().run(argc, argv);

    if (result != 0)
        return result;
    #endif

    // Tests with large matrices for the profiler.
    for (size_t n : {256, 512, 1024, 2048})
        test(n);

    return 0;
}

void test(size_t n)
{
    SquareMatrix a(n);
    SquareMatrix b(n);

    // Arithmetic operations.
    SquareMatrix c;
    c = a + b;
    c = a - b;
    c = a * b;

    // String convertion and construction from a string.
    std::string s = a.toString();
    SquareMatrix d(s);

    // Transpose.
    c = d.transpose();
}

#endif
