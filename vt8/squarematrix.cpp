#include "squarematrix.hpp"

#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <random>
#include <limits>
#include <thread>

/*!
    @file squarematrix.cpp
    @brief SquareMatrix implementation.
*/

// We could generate integers from the whole available range, but the code doesn't check
// for arithmetic overflows, so let's limit the range to avoid arithmetic operations on
// randomly generated matrices overflowing.
const int M_RAND_INT_MIN = -1000000;
const int M_RAND_INT_MAX = 1000000;

const unsigned M_NUM_CORES = std::thread::hardware_concurrency();

const char M_LEFT_BRACKET = '[';
const char M_RIGHT_BRACKET = ']';
const char M_DELIM = ',';

const size_t M_PARSE_FAILURE = 0;

const char *M_ERROR_SIZE_MISMATCH = "SquareMatrix size mismatch.";
const char *M_ERROR_INVALID_STRING = "Invalid string argument to SquareMatrix constructor.";
const char *M_ERROR_NEGATIVE_SIZE = "Trying to construct a SquareMatrix with negative size.";

//! For internal use only. Parse a string into a vector of vectors representing a matrix.
//! @param param1 The vector the numbers are to be read.
//! @param param2 The string they will be read from.
//! @return Modifies the first parameter. Returns the size N of the NxN SquareMatrix (0 on failure, a positive number otherwise).
static size_t constructFromString(std::vector<std::vector<IntElement>> &v, const std::string &s);

//! For internal use only.
//! @return A random integer from the range [M_RAND_INT_MIN, M_RAND_INT_MAX].
static int randInt();

SquareMatrix::SquareMatrix() : n(0) {}

SquareMatrix::SquareMatrix(const std::string &s)
{
    n = constructFromString(elements, s);

    if (n == M_PARSE_FAILURE)
        throw std::invalid_argument(M_ERROR_INVALID_STRING);
}

SquareMatrix::SquareMatrix(int n) : n(n)
{
    // An int is what the specification asked for, so let's go with it instead
    // of unsigned or size_t, but we'll have to throw if it's is negative.
    if (n < 0)
        throw std::invalid_argument(M_ERROR_NEGATIVE_SIZE);

    size_t size = n;

    elements.resize(size);

    if (M_NUM_CORES > 1)
    {
        std::vector<std::thread> threads;
        for (unsigned core=0; core<M_NUM_CORES && core<size; ++core)
            threads.push_back(std::thread(
                [this, size](unsigned offset)
                {
                    for (size_t i=offset; i<size; i+=M_NUM_CORES)
                    {
                        elements[i].reserve(size);
                        for (size_t j=0; j<size; ++j)
                            elements[i].push_back(randInt());
                    }
                }, core));

        for (std::thread &t : threads)
            t.join();
    }
    else
    {
        for (size_t i=0; i<size; ++i)
        {
            elements[i].reserve(size);
            for (size_t j=0; j<size; ++j)
                elements[i].push_back(randInt());
        }
    }
}

SquareMatrix::SquareMatrix(const SquareMatrix &m) : n(m.n), elements(m.elements) {}

SquareMatrix& SquareMatrix::operator=(const SquareMatrix &m)
{
    n = m.n;
    elements = m.elements;
    return *this;
}

SquareMatrix::~SquareMatrix() {}

SquareMatrix SquareMatrix::transpose() const
{
    SquareMatrix transposed;

    std::vector<std::vector<IntElement>> temp(n, std::vector<IntElement>(n));

    size_t size = n;

    if (M_NUM_CORES > 1)
    {
        std::vector<std::thread> threads;
        for (unsigned core=0; core<M_NUM_CORES && core<size; ++core)
            threads.push_back(std::thread(
                [this, &temp, size](unsigned offset)
                {
                    for (size_t i=offset; i<size; i+=M_NUM_CORES)
                        for (size_t j=0; j<size; ++j)
                            temp[i][j] = elements[j][i];
                }, core));

        for (std::thread &t : threads)
            t.join();
    }
    else
    {
        for (size_t i=0; i<size; ++i)
            for (size_t j=0; j<size; ++j)
                temp[i][j] = elements[j][i];
    }

    transposed.n = n;
    transposed.elements = temp;

    return transposed;
}

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix &m)
{
    // Let's assume that the value of n always reflects reality.
    // That is, that the actual size of the vectors never deviates
    // from the size stored in n.
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    size_t size = n;

    if (M_NUM_CORES > 1)
    {
        std::vector<std::thread> threads;
        for (unsigned core=0; core<M_NUM_CORES && core<size; ++core)
            threads.push_back(std::thread(
                [this, &m, size](unsigned offset)
                {
                    for (size_t i=offset; i<size; i+=M_NUM_CORES)
                        for (size_t j=0; j<size; ++j)
                            elements[i][j] += m.elements[i][j];
                }, core));

        for (std::thread &t : threads)
            t.join();
    }
    else
    {
        auto it1 = elements.begin();
        for (auto it2=m.elements.begin(); it1 != elements.end(); ++it1, ++it2)
            std::transform(it1->begin(), it1->end(), it2->begin(), it1->begin(), [](const IntElement &i1, const IntElement &i2){return i1+i2;});
    }

    return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix &m)
{
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    size_t size = n;

    if (M_NUM_CORES > 1)
    {
        std::vector<std::thread> threads;
        for (unsigned core=0; core<M_NUM_CORES && core<size; ++core)
            threads.push_back(std::thread(
                [this, &m, size](unsigned offset)
                {
                    for (size_t i=offset; i<size; i+=M_NUM_CORES)
                        for (size_t j=0; j<size; ++j)
                            elements[i][j] -= m.elements[i][j];
                }, core));

        for (std::thread &t : threads)
            t.join();
    }
    else
    {
        auto it1 = elements.begin();
        for (auto it2=m.elements.begin(); it1 != elements.end(); ++it1, ++it2)
            std::transform(it1->begin(), it1->end(), it2->begin(), it1->begin(), [](const IntElement &i1, const IntElement &i2){return i1-i2;});
    }

    return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix &m)
{
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    size_t size = n;

    auto dotProduct = [this, &m, size](size_t row, size_t col)
    {
        IntElement prod(0);

        for (size_t i=0; i<size; ++i)
            prod += elements[row][i] * m.elements[i][col];

        return prod;
    };

    std::vector<std::vector<IntElement>> temp(size, std::vector<IntElement>(size));

    if (M_NUM_CORES > 1)
    {
        std::vector<std::thread> threads;
        for (unsigned core=0; core<M_NUM_CORES && core<size; ++core)
            threads.push_back(std::thread(
                [this, &m, &temp, size, dotProduct](unsigned offset)
                {
                    for (size_t i=offset; i<size; i+=M_NUM_CORES)
                        for (size_t j=0; j<size; ++j)
                            temp[i][j] = dotProduct(i, j);
                }, core));

        for (std::thread &t : threads)
            t.join();
    }
    else
    {
        for (size_t i=0; i<size; ++i)
            for (size_t j=0; j<size; ++j)
                temp[i][j] = dotProduct(i, j);
    }

    elements = temp;

    return *this;
}

SquareMatrix operator+(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) += m2;
}

SquareMatrix operator-(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) -= m2;
}

SquareMatrix operator*(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) *= m2;
}

bool SquareMatrix::operator==(const SquareMatrix &m) const
{
    return m.elements == elements;
}

void SquareMatrix::print(std::ostream &os) const
{
    os << M_LEFT_BRACKET;

    for (const auto &v : elements)
    {
        os << M_LEFT_BRACKET;

        bool first = true;
        for (const auto i : v)
        {
            if (first)
                first = false;
            else
                os << M_DELIM;

            os << i;
        }

        os << M_RIGHT_BRACKET;
    }

    os << M_RIGHT_BRACKET;
}

std::ostream& operator<<(std::ostream &os, const SquareMatrix &m)
{
    m.print(os);
    return os;
}

std::string SquareMatrix::toString() const
{
    std::stringstream ss;
    print(ss);
    return ss.str();
}

static size_t constructFromString(std::vector<std::vector<IntElement>> &v, const std::string &s)
{
    std::stringstream ss;
    ss << s;

    char ch;
    ch = ss.get();
    if (ch != M_LEFT_BRACKET)
        return M_PARSE_FAILURE;

    size_t sizeN = 0;
    bool sizeFound = false;
    size_t rows = 0;

    for (; ;++rows)
    {
        std::vector<IntElement> row;

        // Once the size is known, allocate enough
        // memory to begin with.
        if (sizeFound)
            row.reserve(sizeN);

        ch = ss.get();
        if (ch != M_LEFT_BRACKET)
        {
            // If no new row is started, the expected
            // character is the final closing bracket.
            if (ch == M_RIGHT_BRACKET)
                break;

            return M_PARSE_FAILURE;
        }

        if (sizeFound && rows>=sizeN)
            return M_PARSE_FAILURE;

        for (size_t n=0; ;)
        {
            // Disallow spaces, tabs, and newlines.
            ch = ss.peek();
            if (ch == ' ' || ch == '\t' || ch == '\n')
                return M_PARSE_FAILURE;

            int value;
            ss >> value;
            if (!ss.good())
                return M_PARSE_FAILURE;

            row.push_back(value);

            ++n;

            ch = ss.get();
            if (ch != M_DELIM)
            {
                if (ch == M_RIGHT_BRACKET)
                {
                    v.push_back(row);

                    if (!sizeFound)
                    {
                        sizeFound = true;
                        sizeN = n;
                        v.reserve(sizeN);
                        break;
                    }
                    else if (n == sizeN)
                        break;

                    return M_PARSE_FAILURE;
                }

                return M_PARSE_FAILURE;
            }
        }
    }

    ch = ss.get();
    if (ch != EOF)
        return M_PARSE_FAILURE;

    // Disallow empty matrices.
    if (!sizeFound)
        return M_PARSE_FAILURE;

    if (rows != sizeN)
        return M_PARSE_FAILURE;

    return sizeN;
}

static int randInt()
{
    static thread_local std::mt19937 prng;

    // Perhaps the range should be more limited, because arithmetic overflows are not handled by the code.
    //std::uniform_int_distribution<int> dist(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());

    std::uniform_int_distribution<int> dist(M_RAND_INT_MIN, M_RAND_INT_MAX);

    return dist(prng);
}
