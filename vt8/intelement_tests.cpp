#include "intelement.hpp"

#include "catch.hpp"

#include <string>
#include <sstream>

TEST_CASE("IntElement misc", "[IntElement]")
{
    CHECK(IntElement(5) == IntElement(5));
    CHECK_FALSE(IntElement(3) == IntElement(5));

    IntElement a;
    a.setVal(4);
    CHECK(a.getVal() == 4);
    CHECK(a == IntElement(4));

    IntElement b(a);
    CHECK(a == b);

    IntElement c(0);
    std::stringstream ss;
    ss << c;
    CHECK(ss.str() == "0");
}

TEST_CASE("IntElement arithmetic", "[IntElement]")
{
    // Testing operator+ suffices, because it is implemented using operator+=
    CHECK(IntElement(4) + IntElement(8) == IntElement(12));
    CHECK(IntElement(9) - IntElement(20) == IntElement(-11));
    CHECK(IntElement(16) * IntElement(16) == IntElement(256));
}
