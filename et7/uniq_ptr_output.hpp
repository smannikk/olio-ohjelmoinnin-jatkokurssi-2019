#ifndef F_UNIQ_PTR_OUTPUT_HPP_INCLUDED
#define F_UNIQ_PTR_OUTPUT_HPP_INCLUDED

#include <ostream>
#include <memory>

//! An overload of the operator<< to print anything pointed at by an std::unique_ptr.
//! @param param1 The output stream as the left operand.
//! @param param2 A unique pointer to any printable object.
//! @return A reference to the output stream.
template <typename T>
std::ostream& operator<<(std::ostream &out, const std::unique_ptr<T> &ptr)
{
    out << *ptr;
    return out;
}

#endif // F_UNIQ_PTR_OUTPUT_HPP_INCLUDED
