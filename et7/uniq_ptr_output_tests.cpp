#include "uniq_ptr_output.hpp"

#include <sstream>
#include <string>
#include <vector>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

template <typename T>
void test(T *ptr, const std::string &result)
{
    std::unique_ptr<T> p(ptr);

    std::stringstream ss;
    ss << p;

    CHECK(ss.str() == result);
}

template <typename T>
std::ostream& operator<<(std::ostream &out, const std::vector<T> &v)
{
    out << "{";

    std::string delim("");
    for (T e : v)
    {
        out << delim << e;
        delim = ",";
    }

    out << "}";

    return out;
}

TEST_CASE("Tests for the overload of operator<< for std::unique_ptr<T>.", "[uniq_ptr_output]")
{
    test(new int(24), "24");
    test(new char('X'), "X");
    test(new std::string("It's in the trees! It's coming!"), "It's in the trees! It's coming!");
    test(new std::vector<int>{1,1,2,3,5,8,13,21}, "{1,1,2,3,5,8,13,21}");
}
