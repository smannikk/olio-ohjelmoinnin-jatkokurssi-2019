#ifndef F_VALUATION_HPP_INCLUDED
#define F_VALUATION_HPP_INCLUDED

#include <map>

/*!
    @file valuation.cpp
    @brief A map from variable names to their values.
*/

//! Define an alternative type name for the map, so that the key type can be
//! more easily changed later if needed.
typedef std::map<char, int> Valuation;

#endif // F_VALUATION_HPP_INCLUDED
