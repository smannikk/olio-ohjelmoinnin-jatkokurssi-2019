#include "greeter.hpp"

Greeter::Greeter(const std::string &g)
{
    greetings.push_back(g);
}

void Greeter::addGreet(const std::string &g)
{
    greetings.push_back(g);
}

std::string Greeter::sayHello() const
{
    std::string allGreetings;

    for (const std::string &g : greetings)
        allGreetings += g + "\n";

    // Assuming that there should be no added newline at the very end.
    if (!allGreetings.empty())
        allGreetings.resize(allGreetings.size()-1);

    return allGreetings;
}
