#ifndef F_SQUAREMATRIX_HPP_INCLUDED
#define F_SQUAREMATRIX_HPP_INCLUDED

/*!
    @file squarematrix.hpp
    @brief Header for the SquareMatrix class and functions related to it.

    A printable NxN integer square matrix that supports basic arithmetic operations.
*/

#include <string>
#include <ostream>

#include "intelement.hpp"

//! A printable NxN integer square matrix that supports basic arithmetic operations.
class SquareMatrix
{
private:
    int n;
    bool transposed;
    std::vector<std::vector<IntElement>> elements;
public:
    //! Default constructor. Doesn't initialize the elements.
    //! @return An uninitialized SquareMatrix
    SquareMatrix();

    //! Constructs the matrix from a string given in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @param A string given in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @return The resulting SquareMatrix
    SquareMatrix(const std::string &s);

    //! Copy constructor.
    //! @param A SquareMatrix to be copied
    //! @return A copy of the input matrix
    SquareMatrix(const SquareMatrix &m);

    //! Destructor. Not functionally necessary, but is required by the interface.
    ~SquareMatrix();

    //! @return Returns a matrix that is the transpose of this matrix.
    SquareMatrix transpose() const;

    //! Adds (element-wise) another matrix to the matrix. Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    SquareMatrix& operator+=(const SquareMatrix &m);

    //! Subtracts (element-wise) another matrix from the matrix. Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    SquareMatrix& operator-=(const SquareMatrix &m);

    //! Multiplies (using matrix-multiplication) the matrix with another matrix.
    //! Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    SquareMatrix& operator*=(const SquareMatrix &m);

    //! Element-wise equality comparison of two matrices.
    //! @param Another SquareMatrix
    //! @return True, if the elements are all equal. False otherwise.
    bool operator==(const SquareMatrix &m) const;

    //! Copy assignment operator
    //! @param param1 The matrix to be assigned to
    //! @param param2 The matrix to be copied
    //! @return A reference to the left operand
    SquareMatrix& operator=(const SquareMatrix &m);

    //! Outputs the matrix to the stream in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @param An output stream where the matrix is to be output
    void print(std::ostream& os) const;

    //! Returns the matrix as a string in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @return A string containing a printed version of the matrix
    std::string toString() const;
};

//! Outputs the matrix to the stream in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
//! @return Returns a reference to the stream.
//! @param param1 An output stream
//! @param param2 The SquareMatrix to be output to the stream
std::ostream& operator<<(std::ostream &os, const SquareMatrix &m);

//! Adds (element-wise) two matrices together.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A SquareMatrix
//! @param param2 A SquareMatrix
SquareMatrix operator+(const SquareMatrix &m1, const SquareMatrix &m2);

//! Subtracts (element-wise) a matrix from another.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A SquareMatrix
//! @param param2 A SquareMatrix
SquareMatrix operator-(const SquareMatrix &m1, const SquareMatrix &m2);

//! Multiplies (using matrix-multiplication) two matrices.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A SquareMatrix
//! @param param2 A SquareMatrix
SquareMatrix operator*(const SquareMatrix &m1, const SquareMatrix &m2);

#endif // F_SQUAREMATRIX_HPP_INCLUDED
