#include "squarematrix.hpp"

#include "catch.hpp"

#include <string>
#include <sstream>

TEST_CASE("2x2 square matrix printing", "[squarematrix-string]")
{
    SquareMatrix a{1, 2, 3, 4};
    CHECK(a.toString() == "[[1,2][3,4]]");

    std::stringstream ss;
    ss << a;
    CHECK(ss.str() == "[[1,2][3,4]]");
}

TEST_CASE("2x2 square matrix arithmetic", "[squarematrix-math]")
{
    // Testing operator+ suffices, because it is implemented using operator+=
    CHECK(SquareMatrix{1, 2, 3, 4} + SquareMatrix{4, 3, 2, 1} == SquareMatrix{5, 5, 5, 5});
    CHECK(SquareMatrix{2, 2, 2, 2} - SquareMatrix{1, 2, 3, 4} == SquareMatrix{1, 0, -1, -2});
    CHECK(SquareMatrix{1, 2, 3, 4} * SquareMatrix{5, 6, 7, 8} == SquareMatrix{1*5+2*7, 1*6+2*8, 3*5+4*7, 3*6+4*8});
}

TEST_CASE("2x2 square matrix constructors", "[squarematrix-math]")
{
    // Check the comparison operator
    CHECK(SquareMatrix{1, 2, 4, 5} == SquareMatrix{1, 2, 4, 5});
    CHECK_FALSE(SquareMatrix{0, 0, 0, 1} == SquareMatrix{0, 0, 0, 0});

    // Check the copy constructor
    SquareMatrix a{1, 2, 3, 4};
    SquareMatrix b(a);
    CHECK(a == b);

    // Check assignment
    SquareMatrix c;
    c = a;  // The assignment operator is automatically created.
    CHECK(a == c);
}
