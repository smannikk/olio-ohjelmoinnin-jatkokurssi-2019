#include "squarematrix.hpp"

/*!
    @file squarematrix.cpp
    @brief SquareMatrix implementation.
*/

#include <sstream>

SquareMatrix::SquareMatrix() {}
SquareMatrix::~SquareMatrix() {}

SquareMatrix::SquareMatrix(const IntElement &i11, const IntElement &i12, const IntElement &i21, const IntElement &i22) :
e11(i11),
e12(i12),
e21(i21),
e22(i22)
{}

// Since the class doesn't manage any resources (such as dynamically allocated memory or file handlers),
// the automatically created copy constructor would suffice, but let's define it explicitly because it
// is included in the class diagram for the exercise.
// For the same reason, there's no need to follow the rule of three and have a custom-made assignment operator
// (even though we do have a self-written copy constructor). Nor is there need for a move constructor or a move
// assignment operator.
SquareMatrix::SquareMatrix(const SquareMatrix &m) : SquareMatrix(m.e11, m.e12, m.e21, m.e22) {}

void SquareMatrix::print(std::ostream &os) const
{
    // The size of the matrix (2x2) is hardcoded, so this suffices.
    os << "[[" << e11 << "," << e12 << "][" << e21 << "," << e22 << "]]";
}

std::ostream& operator<<(std::ostream &os, const SquareMatrix &m)
{
    m.print(os);
    return os;
}

std::string SquareMatrix::toString() const
{
    std::stringstream ss;
    print(ss);
    return ss.str();
}

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix &m)
{
    e11 += m.e11;
    e12 += m.e12;
    e21 += m.e21;
    e22 += m.e22;

    return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix &m)
{
    e11 -= m.e11;
    e12 -= m.e12;
    e21 -= m.e21;
    e22 -= m.e22;

    return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix &m)
{
    // The size is hardcoded, so let's just go with this:
    SquareMatrix temp(*this);
    e11 = temp.e11*m.e11 + temp.e12*m.e21;
    e12 = temp.e11*m.e12 + temp.e12*m.e22;
    e21 = temp.e21*m.e11 + temp.e22*m.e21;
    e22 = temp.e21*m.e12 + temp.e22*m.e22;

    return *this;
}

SquareMatrix operator+(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) += m2;
}

SquareMatrix operator-(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) -= m2;
}

SquareMatrix operator*(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) *= m2;
}

bool SquareMatrix::operator==(const SquareMatrix &m) const
{
    return e11==m.e11 && e12==m.e12 && e21==m.e21 && e22==m.e22;
}
