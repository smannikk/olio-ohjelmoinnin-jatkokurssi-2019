#ifndef F_SQUAREMATRIX_HPP_INCLUDED
#define F_SQUAREMATRIX_HPP_INCLUDED

/*!
    @file squarematrix.hpp
    @brief Header for the SquareMatrix class and functions related to it.

    A printable 2x2 integer square matrix that supports basic arithmetic operations.
*/

#include <string>
#include <ostream>

#include "intelement.hpp"

//! A printable 2x2 integer square matrix that supports basic arithmetic operations.
class SquareMatrix
{
private:
    IntElement e11, e12, e21, e22;
public:
    //! Default constructor. Doesn't initialize the elements.
    //! @return An uninitialized SquareMatrix
    SquareMatrix();

    //! Constructs the matrix from the given elements.
    //! @param param1 Element (1, 1)
    //! @param param2 Element (1, 2)
    //! @param param3 Element (2, 1)
    //! @param param4 Element (2, 2)
    //! @return The resulting SquareMatrix
    SquareMatrix(const IntElement &i11, const IntElement &i12, const IntElement &i21, const IntElement &i22);

    //! Copy constructor. Not functionally necessary (the automatically generated constructor would suffice),
    //! but the interface requires this so let's have it here explicitly.
    //! @param A SquareMatrix to be copied
    //! @return A copy of the input matrix
    SquareMatrix(const SquareMatrix &m);

    //! Destructor. Not functionally necessary, but is required by the interface.
    ~SquareMatrix();

    //! Adds (element-wise) another matrix to the matrix. Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    SquareMatrix& operator+=(const SquareMatrix &m);

    //! Subtracts (element-wise) another matrix from the matrix. Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    SquareMatrix& operator-=(const SquareMatrix &m);

    //! Multiplies (using matrix-multiplication) the matrix with another matrix.
    //! Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    SquareMatrix& operator*=(const SquareMatrix &m);

    //! Element-wise equality comparison of two matrices.
    //! @param Another SquareMatrix
    //! @return True, if the elements are all equal. False otherwise.
    bool operator==(const SquareMatrix &m) const;

    //! Outputs the matrix to the stream in the format: [[1,2][3,4]]
    //! @param An output stream where the matrix is to be output
    void print(std::ostream& os) const;

    //! Returns the matrix as a string in the format: [[1,2][3,4]]
    //! @return A string containing a printed version of the matrix
    std::string toString() const;
};

//! Outputs the matrix to the stream in the format: [[1,2][3,4]]
//! @return Returns a reference to the stream.
//! @param param1 An output stream
//! @param param2 The SquareMatrix to be output to the stream
std::ostream& operator<<(std::ostream &os, const SquareMatrix &m);

//! Adds (element-wise) two matrices together.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A SquareMatrix
//! @param param2 A SquareMatrix
SquareMatrix operator+(const SquareMatrix &m1, const SquareMatrix &m2);

//! Subtracts (element-wise) a matrix from another.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A SquareMatrix
//! @param param2 A SquareMatrix
SquareMatrix operator-(const SquareMatrix &m1, const SquareMatrix &m2);

//! Multiplies (using matrix-multiplication) two matrices.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A SquareMatrix
//! @param param2 A SquareMatrix
SquareMatrix operator*(const SquareMatrix &m1, const SquareMatrix &m2);

#endif // F_SQUAREMATRIX_HPP_INCLUDED
