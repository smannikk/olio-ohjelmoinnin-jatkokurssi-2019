#ifndef F_CONCRETESQUAREMATRIX_HPP_INCLUDED
#define F_CONCRETESQUAREMATRIX_HPP_INCLUDED

/*!
    @file concretematrix.hpp
    @brief Header for the SquareMatrix class and functions related to it.

    A printable NxN integer square matrix that supports basic arithmetic operations.
*/

#include <vector>
#include <memory>

#include "intelement.hpp"

//! A printable NxN integer square matrix that supports basic arithmetic operations.
class ConcreteSquareMatrix
{
private:
    unsigned n;
    std::vector<std::vector<std::unique_ptr<IntElement>>> elements;
public:
    //! Default constructor.
    //! @return An empty ConcreteSquareMatrix
    ConcreteSquareMatrix();

    //! Constructs the matrix from a string given in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @param A string given in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @return The resulting ConcreteSquareMatrix
    //! @throw std::invalid_argument if a ConcreteSquareMatrix cannot be constructed from the string.
    ConcreteSquareMatrix(const std::string &s);

    //! Copy constructor
    //! @param A ConcreteSquareMatrix to be copied
    //! @return A copy of the input matrix
    ConcreteSquareMatrix(const ConcreteSquareMatrix &m);

    //! Move constructor
    //! @param A ConcreteSquareMatrix to be moved
    //! @return A ConcreteSquareMatrix
    ConcreteSquareMatrix(ConcreteSquareMatrix &&m);

    //! Copy assignment operator
    //! @param param1 The matrix to be assigned to
    //! @param param2 The matrix to be copied
    //! @return A reference to the left operand
    ConcreteSquareMatrix& operator=(const ConcreteSquareMatrix &m);

    //! Move assignment operator
    //! @param param1 The matrix to be assigned to
    //! @param param2 The matrix to be moved
    //! @return A reference to the left operand
    ConcreteSquareMatrix& operator=(ConcreteSquareMatrix &&m);

    //! Destructor.
    ~ConcreteSquareMatrix();

    //! @return Returns a matrix that is the transpose of this matrix.
    ConcreteSquareMatrix transpose() const;

    //! Adds (element-wise) another matrix to the matrix. Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    ConcreteSquareMatrix& operator+=(const ConcreteSquareMatrix &m);

    //! Subtracts (element-wise) another matrix from the matrix. Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    ConcreteSquareMatrix& operator-=(const ConcreteSquareMatrix &m);

    //! Multiplies (using matrix-multiplication) the matrix with another matrix.
    //! Modifies the original matrix.
    //! @param param1 The matrix to be modified.
    //! @param param2 A matrix to be used as the right operand
    //! @return A reference to the left operand
    //! @throw std::invalid_argument if the sizes of the matrices don't match.
    ConcreteSquareMatrix& operator*=(const ConcreteSquareMatrix &m);

    //! Element-wise equality comparison of two matrices.
    //! @param Another ConcreteSquareMatrix
    //! @return True, if the elements are all equal. False otherwise.
    bool operator==(const ConcreteSquareMatrix &m) const;

    //! Outputs the matrix to the stream in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @param An output stream where the matrix is to be output
    void print(std::ostream& os) const;

    //! Returns the matrix as a string in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
    //! @return A string containing a printed version of the matrix
    std::string toString() const;
};

//! Outputs the matrix to the stream in the format: [[a11,a12,...,a1n][a21,a22,...,a2n]...[an1,an2,...,ann]]
//! @return Returns a reference to the stream.
//! @param param1 An output stream
//! @param param2 The ConcreteSquareMatrix to be output to the stream
std::ostream& operator<<(std::ostream &os, const ConcreteSquareMatrix &m);

//! Adds (element-wise) two matrices together.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A ConcreteSquareMatrix
//! @param param2 A ConcreteSquareMatrix
//! @throw std::invalid_argument if the sizes of the matrices don't match.
ConcreteSquareMatrix operator+(const ConcreteSquareMatrix &m1, const ConcreteSquareMatrix &m2);

//! Subtracts (element-wise) a matrix from another.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A ConcreteSquareMatrix
//! @param param2 A ConcreteSquareMatrix
//! @throw std::invalid_argument if the sizes of the matrices don't match.
ConcreteSquareMatrix operator-(const ConcreteSquareMatrix &m1, const ConcreteSquareMatrix &m2);

//! Multiplies (using matrix-multiplication) two matrices.
//! The original matrices are not modified (no side effects).
//! @return Returns the resulting matrix.
//! @param param1 A ConcreteSquareMatrix
//! @param param2 A ConcreteSquareMatrix
//! @throw std::invalid_argument if the sizes of the matrices don't match.
ConcreteSquareMatrix operator*(const ConcreteSquareMatrix &m1, const ConcreteSquareMatrix &m2);

#endif // F_CONCRETESQUAREMATRIX_HPP_INCLUDED
