#include "greeter.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <utility>

TEST_CASE("Greeter test", "[greeter]")
{
    Greeter a(new HelloGreet);
    CHECK(a.sayHello() == "Hello!");

    a.addGreet(new MoroGreet);
    CHECK(a.sayHello() == "Hello!\nMoro!");
}
