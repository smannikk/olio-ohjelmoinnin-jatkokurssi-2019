#include "greeter.hpp"

#include <utility>

Greeter::Greeter(Greet *g)
{
    greetings.push_back(g);
}

Greeter::~Greeter()
{
    for (Greet* const g : greetings)
        delete g;
}

void Greeter::addGreet(Greet *g)
{
    greetings.push_back(g);
}

std::string Greeter::sayHello() const
{
    std::string allGreetings;

    for (Greet* const g : greetings)
        allGreetings += g->greet() + "\n";

    // Assuming that there should be no added newline at the very end.
    if (!allGreetings.empty())
        allGreetings.resize(allGreetings.size()-1);

    return allGreetings;
}

std::string HelloGreet::greet() const
{
    return "Hello!";
}

std::string MoroGreet::greet() const
{
    return "Moro!";
}

Greet::~Greet() {}
