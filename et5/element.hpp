#ifndef F_ELEMENT_HPP_INCLUDED
#define F_ELEMENT_HPP_INCLUDED

#include <string>
#include <ostream>

#include "valuation.hpp"

class Element
{
public:
    virtual ~Element();
    virtual Element* clone() const = 0;
    virtual std::string toString() const = 0;
    virtual int evaluate(const Valuation &val) const = 0;
};

std::ostream& operator<<(std::ostream &os, const Element &i);

#endif // F_ELEMENT_HPP_INCLUDED
