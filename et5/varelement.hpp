#ifndef F_VARELEMENT_HPP_INCLUDED
#define F_VARELEMENT_HPP_INCLUDED

#include "element.hpp"

class VariableElement : public Element
{
private:
    char var;
public:
    VariableElement();
    VariableElement(char v);
    ~VariableElement();

    char getVal() const;
    void setVal(char v);

    Element* clone() const;
    std::string toString() const;
    int evaluate(const Valuation &val) const;

    bool operator==(const VariableElement &i) const;
};

#endif // F_VARELEMENT_HPP_INCLUDED
