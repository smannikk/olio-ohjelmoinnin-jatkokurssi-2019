#ifndef F_GREETER_HPP_INCLUDED
#define F_GREETER_HPP_INCLUDED

#include <string>
#include <vector>

class Greet
{
public:
    virtual ~Greet();
    virtual std::string greet() const = 0;
};

class HelloGreet : public Greet
{
    std::string greet() const;
};

class MoroGreet : public Greet
{
    std::string greet() const;
};

class Greeter
{
private:
    std::vector<Greet*> greetings;
public:
    Greeter(Greet *g);

    ~Greeter();

    void addGreet(Greet *g);
    std::string sayHello() const;
};

#endif // F_GREETER_HPP_INCLUDED
