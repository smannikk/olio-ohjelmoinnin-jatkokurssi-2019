#include "greeter.hpp"

Greeter::Greeter(const std::string &greeting) : greetings(greeting) {}

std::string Greeter::sayHello() const
{
    return greetings;
}
