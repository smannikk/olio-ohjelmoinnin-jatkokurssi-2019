#include "greeter.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE("Greeter test", "[greeter]")
{
    std::string testString = "Hello, World!";
    Greeter test(testString);
    CHECK(test.sayHello() == testString);
}
