#ifndef F_INTELEMENT_HPP_INCLUDED
#define F_INTELEMENT_HPP_INCLUDED

#include <ostream>

class IntElement
{
private:
    int val;
public:
    IntElement();
    IntElement(int v);
    ~IntElement();

    int getVal() const;
    void setVal(int v);

    IntElement& operator+=(const IntElement &i);
    IntElement& operator-=(const IntElement &i);
    IntElement& operator*=(const IntElement &i);
};

IntElement operator+(const IntElement &i1, const IntElement &i2);
IntElement operator-(const IntElement &i1, const IntElement &i2);
IntElement operator*(const IntElement &i1, const IntElement &i2);
std::ostream& operator<<(std::ostream &os, const IntElement &i);

#endif // F_INTELEMENT_HPP_INCLUDED
