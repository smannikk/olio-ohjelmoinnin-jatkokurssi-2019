#ifndef F_GREETER_HPP_INCLUDED
#define F_GREETER_HPP_INCLUDED

#include <string>

class Greeter
{
private:
    std::string greetings;
public:
    Greeter(const std::string &greeting);
    std::string sayHello() const;
};

#endif // F_GREETER_HPP_INCLUDED
