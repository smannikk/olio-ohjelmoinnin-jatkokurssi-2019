#ifndef F_SQUAREMATRIX_HPP_INCLUDED
#define F_SQUAREMATRIX_HPP_INCLUDED

#include <string>
#include <ostream>

#include "intelement.hpp"

// A 2x2 integer square matrix
class SquareMatrix
{
private:
    IntElement e11, e12, e21, e22;
public:
    SquareMatrix();
    SquareMatrix(const IntElement &i11, const IntElement &i12, const IntElement &i21, const IntElement &i22);
    SquareMatrix(const SquareMatrix &m);
    ~SquareMatrix();

    SquareMatrix& operator+=(const SquareMatrix &m);
    SquareMatrix& operator-=(const SquareMatrix &m);
    SquareMatrix& operator*=(const SquareMatrix &m);

    // Outputs the matrix to the stream in the format: [[1,2][3,4]]
    void print(std::ostream& os) const;

    // Returns the matrix as a string in the format: [[1,2][3,4]]
    std::string toString() const;
};

// Outputs the matrix to the stream in the format: [[1,2][3,4]]
std::ostream& operator<<(std::ostream &os, const SquareMatrix &m);

SquareMatrix operator+(const SquareMatrix &m1, const SquareMatrix &m2);
SquareMatrix operator-(const SquareMatrix &m1, const SquareMatrix &m2);
SquareMatrix operator*(const SquareMatrix &m1, const SquareMatrix &m2);

#endif // F_SQUAREMATRIX_HPP_INCLUDED
