#include "intelement.hpp"

#include <sstream>

/*!
    @file intelement.cpp
    @brief IntElement implementation.
*/

IntElement::IntElement() : val(0) {}
IntElement::~IntElement() {}

IntElement::IntElement(int v) : val(v) {}

int IntElement::getVal() const
{
    return val;
}

void IntElement::setVal(int v)
{
    val = v;
}

IntElement& IntElement::operator+=(const IntElement &i)
{
    val += i.val;

    return *this;
}

IntElement& IntElement::operator-=(const IntElement &i)
{
    val -= i.val;

    return *this;
}

IntElement& IntElement::operator*=(const IntElement &i)
{
    val *= i.val;

    return *this;
}

IntElement operator+(const IntElement &i1, const IntElement &i2)
{
    return IntElement(i1) += i2;
}

IntElement operator-(const IntElement &i1, const IntElement &i2)
{
    return IntElement(i1) -= i2;
}

IntElement operator*(const IntElement &i1, const IntElement &i2)
{
    return IntElement(i1) *= i2;
}

bool IntElement::operator==(const IntElement &i) const
{
    return val == i.val;
}

bool IntElement::operator!=(const IntElement &i) const
{
    return !(*this == i);
}

Element* IntElement::clone() const
{
    return new IntElement(*this);
}

int IntElement::evaluate(const Valuation &val) const
{
    return getVal();
}

std::string IntElement::toString() const
{
    std::stringstream ss;
    ss << val;
    return ss.str();
}
