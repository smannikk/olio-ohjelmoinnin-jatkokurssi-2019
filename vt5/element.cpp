#include "element.hpp"

/*!
    @file element.cpp
    @brief Implementation for some of the parts of the abstract Element.
*/

Element::~Element() {}

std::ostream& operator<<(std::ostream &os, const Element &i)
{
    return (os << i.toString());
}
