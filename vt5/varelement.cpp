#include "varelement.hpp"

#include <sstream>

/*!
    @file varelement.cpp
    @brief VarElement implementation.
*/

VariableElement::VariableElement() : var('\0') {}
VariableElement::~VariableElement() {}

VariableElement::VariableElement(char v) : var(v) {}

char VariableElement::getVal() const
{
    return var;
}

void VariableElement::setVal(char v)
{
    var = v;
}

Element* VariableElement::clone() const
{
    return new VariableElement(*this);
}

std::string VariableElement::toString() const
{
    std::stringstream ss;
    ss << var;
    return ss.str();
}

int VariableElement::evaluate(const Valuation &val) const
{
    return val.at(var);
}

bool VariableElement::operator==(const VariableElement &i) const
{
    return var == i.var;
}

bool VariableElement::operator!=(const VariableElement &i) const
{
    return !(*this == i);
}
