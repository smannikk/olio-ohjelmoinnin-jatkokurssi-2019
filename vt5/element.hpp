#ifndef F_ELEMENT_HPP_INCLUDED
#define F_ELEMENT_HPP_INCLUDED

#include <string>
#include <ostream>

#include "valuation.hpp"

/*!
    @file element.hpp
    @brief Header for the Element class and functions related to it.
*/

//! An abstract base class for an element held in a matrix.
class Element
{
public:
    //! Virtual destructor.
    virtual ~Element();

    //! Pure virtual cloning function.
    virtual Element* clone() const = 0;

    //! Pure virtual string convertion function.
    virtual std::string toString() const = 0;

    //! Pure virtual function for evaluating the value held in an element as an integer.
    virtual int evaluate(const Valuation &val) const = 0;
};

//! Prints the value of an object whose class is derived from Element into the stream.
//! Returns a reference to the stream.
//! @param param1 An output stream.
//! @param param2 An object whose type is derived from Element to be output to the stream.
//! @return A reference to the output stream.
std::ostream& operator<<(std::ostream &os, const Element &i);

#endif // F_ELEMENT_HPP_INCLUDED
