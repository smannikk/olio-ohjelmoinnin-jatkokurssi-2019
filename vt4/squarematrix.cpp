#include "squarematrix.hpp"

#include <sstream>
#include <stdexcept>
#include <algorithm>

/*!
    @file squarematrix.cpp
    @brief SquareMatrix implementation.
*/

const char M_LEFT_BRACKET = '[';
const char M_RIGHT_BRACKET = ']';
const char M_DELIM = ',';

const size_t M_PARSE_FAILURE = 0;

// A custom exception class could be inherited from std::invalid_argument for both cases,
// but since they should never be thrown from the same function it doesn't seem absolutely
// necessary to be able to differentiate between them on the type level.
const char *M_ERROR_SIZE_MISMATCH = "SquareMatrix size mismatch.";
const char *M_ERROR_INVALID_STRING = "Invalid string argument to SquareMatrix constructor.";

//! For internal use only. Parse a string into a vector of vectors representing a matrix.
//! @param param1 The vector the numbers are to be read.
//! @param param2 The string they will be read from.
//! @return Modifies the first parameter. Returns the size N of the NxN SquareMatrix (0 on failure, a positive number otherwise).
static size_t constructFromString(std::vector<std::vector<std::unique_ptr<IntElement>>> &v, const std::string &s);

SquareMatrix::SquareMatrix() : n(0) {}

SquareMatrix::SquareMatrix(const std::string &s)
{
    n = constructFromString(elements, s);

    if (n == M_PARSE_FAILURE)
        throw std::invalid_argument(M_ERROR_INVALID_STRING);
}

SquareMatrix::SquareMatrix(const SquareMatrix &m) : n(m.n)
{
    size_t size = n;

    elements.resize(size);
    for (size_t i=0; i<size; ++i)
    {
        elements.reserve(size);
        for (size_t j=0; j<size; ++j)
            elements[i].push_back(std::unique_ptr<IntElement>(m.elements[i][j]->clone()));
    }
}

SquareMatrix::SquareMatrix(SquareMatrix &&m) : n(m.n), elements(std::move(m.elements)) {}

SquareMatrix& SquareMatrix::operator=(const SquareMatrix &m)
{
    if (this != &m)
        *this = std::move(SquareMatrix(m));

    return *this;
}

SquareMatrix& SquareMatrix::operator=(SquareMatrix &&m)
{
    if (this != &m)
    {
        n = m.n;
        elements = std::move(m.elements);
    }

    return *this;
}

SquareMatrix::~SquareMatrix() {}

// Transposing could be done by just copying the matrix and flipping a switch that makes
// the indexing switch from [row][column] to [column][row], but since we have a vector
// of vectors, it is simpler to be able to treat a single vector as a single row, so we
// won't go with that approach.
SquareMatrix SquareMatrix::transpose() const
{
    SquareMatrix transposed;

    size_t size = n;

    std::vector<std::vector<std::unique_ptr<IntElement>>> temp;
    temp.resize(size);

    for (size_t i=0; i<size; ++i)
    {
        temp[i].reserve(size);
        for (size_t j=0; j<size; ++j)
            temp[i].push_back(std::unique_ptr<IntElement>(elements[j][i]->clone()));
    }

    transposed.n = n;
    transposed.elements = std::move(temp);

    return transposed;
}

SquareMatrix& SquareMatrix::operator+=(const SquareMatrix &m)
{
    // Let's assume that the value of n always reflects reality.
    // That is, that the actual size of the vectors never deviates
    // from the size stored in n.
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    auto it1 = elements.begin();
    for (auto it2=m.elements.begin(); it1 != elements.end(); ++it1, ++it2)
        std::transform(it1->begin(), it1->end(), it2->begin(), it1->begin(),
                       [](const std::unique_ptr<IntElement> &i1, const std::unique_ptr<IntElement> &i2){return std::unique_ptr<IntElement>(new IntElement(*i1+*i2));});

    return *this;
}

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix &m)
{
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    auto it1 = elements.begin();
    for (auto it2=m.elements.begin(); it1 != elements.end(); ++it1, ++it2)
        std::transform(it1->begin(), it1->end(), it2->begin(), it1->begin(),
                       [](const std::unique_ptr<IntElement> &i1, const std::unique_ptr<IntElement> &i2){return std::unique_ptr<IntElement>(new IntElement(*i1-*i2));});

    return *this;
}

SquareMatrix& SquareMatrix::operator*=(const SquareMatrix &m)
{
    if (n != m.n)
        throw std::invalid_argument(M_ERROR_SIZE_MISMATCH);

    size_t size = n;

    auto dotProduct = [this, &m, size](size_t row, size_t col)
    {
        IntElement prod(0);

        for (size_t i=0; i<size; ++i)
            prod += *elements[row][i] * *m.elements[i][col];

        return prod;
    };

    std::vector<std::vector<std::unique_ptr<IntElement>>> temp;
    temp.resize(size);

    for (size_t i=0; i<size; ++i)
    {
        temp[i].reserve(size);
        for (size_t j=0; j<size; ++j)
            temp[i].push_back(std::unique_ptr<IntElement>(new IntElement(dotProduct(i, j))));
    }

    elements = std::move(temp);

    return *this;
}

SquareMatrix operator+(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) += m2;
}

SquareMatrix operator-(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) -= m2;
}

SquareMatrix operator*(const SquareMatrix &m1, const SquareMatrix &m2)
{
    return SquareMatrix(m1) *= m2;
}

bool SquareMatrix::operator==(const SquareMatrix &m) const
{
    if (n != m.n)
        return false;

    size_t size = n;
    for (size_t i=0; i<size; ++i)
        for (size_t j=0; j<size; ++j)
            if (*elements[i][j] != *m.elements[i][j])
                return false;

    return true;
}

void SquareMatrix::print(std::ostream &os) const
{
    os << M_LEFT_BRACKET;

    for (const auto &v : elements)
    {
        os << M_LEFT_BRACKET;

        bool first = true;
        for (const auto &i : v)
        {
            if (first)
                first = false;
            else
                os << M_DELIM;

            os << *i;
        }

        os << M_RIGHT_BRACKET;
    }

    os << M_RIGHT_BRACKET;
}

std::ostream& operator<<(std::ostream &os, const SquareMatrix &m)
{
    m.print(os);
    return os;
}

std::string SquareMatrix::toString() const
{
    std::stringstream ss;
    print(ss);
    return ss.str();
}

static size_t constructFromString(std::vector<std::vector<std::unique_ptr<IntElement>>> &v, const std::string &s)
{
    std::stringstream ss;
    ss << s;

    char ch;
    ch = ss.get();
    if (ch != M_LEFT_BRACKET)
        return M_PARSE_FAILURE;

    size_t sizeN = 0;
    bool sizeFound = false;
    size_t rows = 0;

    for (; ;++rows)
    {
        std::vector<std::unique_ptr<IntElement>> row;

        // Once the size is known, allocate enough
        // memory to begin with.
        if (sizeFound)
            row.reserve(sizeN);

        ch = ss.get();
        if (ch != M_LEFT_BRACKET)
        {
            // If no new row is started, the expected
            // character is the final closing bracket.
            if (ch == M_RIGHT_BRACKET)
                break;

            return M_PARSE_FAILURE;
        }

        if (sizeFound && rows>=sizeN)
            return M_PARSE_FAILURE;

        for (size_t n=0; ;)
        {
            // Disallow spaces, tabs, and newlines.
            ch = ss.peek();
            if (ch == ' ' || ch == '\t' || ch == '\n')
                return M_PARSE_FAILURE;

            int value;
            ss >> value;
            if (!ss.good())
                return M_PARSE_FAILURE;

            row.push_back(std::unique_ptr<IntElement>(new IntElement(value)));

            ++n;

            ch = ss.get();
            if (ch != M_DELIM)
            {
                if (ch == M_RIGHT_BRACKET)
                {
                    v.push_back(std::move(row));

                    if (!sizeFound)
                    {
                        sizeFound = true;
                        sizeN = n;
                        v.reserve(sizeN);
                        break;
                    }
                    else if (n == sizeN)
                        break;

                    return M_PARSE_FAILURE;
                }

                return M_PARSE_FAILURE;
            }
        }
    }

    ch = ss.get();
    if (ch != EOF)
        return M_PARSE_FAILURE;

    // Disallow empty matrices.
    if (!sizeFound)
        return M_PARSE_FAILURE;

    if (rows != sizeN)
        return M_PARSE_FAILURE;

    return sizeN;
}
