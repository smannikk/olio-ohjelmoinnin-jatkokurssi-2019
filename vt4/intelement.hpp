#ifndef F_INTELEMENT_HPP_INCLUDED
#define F_INTELEMENT_HPP_INCLUDED

/*!
    @file intelement.hpp
    @brief Header for the IntElement class and functions related to it.

    Abstraction and encapsulation for a single integer...
*/

#include <ostream>

//! A class that holds a single integer value. Supports basic arithmetic and is printable.
class IntElement
{
private:
    int val;
public:
    //! Default constructor. Doesn't initialize the element.
    //! @return An uninitialized IntElement
    IntElement();

    //! Constructs the element from the given integer value.
    //! @param An integer value to be held in the element
    //! @return An initialized IntElement
    IntElement(int v);

    //! Destructor. Not functionally necessary, but is required by the interface.
    ~IntElement();

    //! A simple getter for the value.
    //! @return The value of the element
    int getVal() const;

    //! A simple setter for the value.
    //! @param The new value for the element
    void setVal(int v);

    //! Adds to the value another IntElement's value. Modifies the original element.
    //! @param param1 The element to be modified
    //! @param param2 The element to be added
    //! @return A reference to the left operand
    IntElement& operator+=(const IntElement &i);

    //! Subtracts from the value another IntElement's value. Modifies the original element.
    //! @param param1 The element to be modified
    //! @param param2 The element to be subtracted
    //! @return A reference to the left operand
    IntElement& operator-=(const IntElement &i);

    //! Multiplies the value with another IntElement's value. Modifies the original element.
    //! @param param1 The element to be modified
    //! @param param2 The element to be multiplied with
    //! @return A reference to the left operand
    IntElement& operator*=(const IntElement &i);

    //! Compares two IntElements by value.
    //! @return true, if they are equal, false otherwise
    bool operator==(const IntElement &i) const;

    //! Compares two IntElements by value to see if they are inequal.
    //! @return true, if they are not equal, false otherwise
    bool operator!=(const IntElement &i) const;

    //! Create a dynamically allocated copy of the instance.
    //! @return A pointer to a dynamically allocated IntElement object.
    IntElement* clone() const;
};

//! Adds two IntElements.
//! No side effects (the operands are not modified).
//! @param param1 An IntElement
//! @param param2 An IntElement
//! @result The resulting IntElement
IntElement operator+(const IntElement &i1, const IntElement &i2);

//! Subtracts an IntElement from another.
//! No side effects (the operands are not modified).
//! @param param1 An IntElement
//! @param param2 An IntElement
//! @result The resulting IntElement
IntElement operator-(const IntElement &i1, const IntElement &i2);

//! Multiplies two IntElements.
//! No side effects (the operands are not modified).
//! @param param1 An IntElement
//! @param param2 An IntElement
//! @result The resulting IntElement
IntElement operator*(const IntElement &i1, const IntElement &i2);

//! Prints the value of the IntElement into the stream.
//! Returns a reference to the stream.
//! @param param1 An output stream
//! @param param2 The IntElement to be output to the stream
//! @return A reference to the output stream
std::ostream& operator<<(std::ostream &os, const IntElement &i);

#endif // F_INTELEMENT_HPP_INCLUDED
