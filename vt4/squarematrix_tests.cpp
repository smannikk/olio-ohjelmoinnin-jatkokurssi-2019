#include "squarematrix.hpp"

#include "catch.hpp"

#include <sstream>
#include <stdexcept>
#include <utility>

const std::vector<std::string> g_testStrings = {"[[1]]", "[[1,2][4,5]]", "[[-1,2][4,-5]]",
                                                "[[0,0][0,0]]", "[[1,2,3][4,5,6][7,8,9]]",
                                                "[[112323,-43432,33434][434,-434345,0][7,8,9]]",
                                                "[[1,2,3,1,2,3][4,5,6,1,2,3][7,8,9,1,2,3][1,2,3,1,2,3][4,5,6,1,2,3][7,8,9,1,2,3]]"};

TEST_CASE("SquareMatrix IO", "[SquareMatrix]")
{
    for (const std::string &s : g_testStrings)
    {
        SquareMatrix a(s);

        CHECK(a.toString() == s);

        std::stringstream ss;
        ss << a;
        CHECK(ss.str() == s);
    }

    CHECK(SquareMatrix("[[+0,0][0,0]]").toString() == "[[0,0][0,0]]");
}

TEST_CASE("SquareMatrix arithmetic", "[SquareMatrix]")
{
    // Testing operator+ suffices, because it is implemented using operator+=
    CHECK(SquareMatrix("[[1,2][3,4]]") + SquareMatrix("[[4,3][2,1]]") == SquareMatrix("[[5,5][5,5]]"));
    CHECK(SquareMatrix("[[2,2][2,2]]") - SquareMatrix("[[1,2][3,4]]") == SquareMatrix("[[1,0][-1,-2]]"));
    CHECK(SquareMatrix("[[1,2][3,4]]") * SquareMatrix("[[5,6][7,8]]") == SquareMatrix("[[19,22][43,50]]"));

    CHECK(SquareMatrix("[[1]]") + SquareMatrix("[[2]]") == SquareMatrix("[[3]]"));
    CHECK(SquareMatrix("[[1]]") - SquareMatrix("[[2]]") == SquareMatrix("[[-1]]"));
    CHECK(SquareMatrix("[[5]]") * SquareMatrix("[[2]]") == SquareMatrix("[[10]]"));

    CHECK(SquareMatrix("[[1,2,3][4,5,6][7,8,9]]") + SquareMatrix("[[4,5,7][8,6,9][1,1,2]]") == SquareMatrix("[[5,7,10][12,11,15][8,9,11]]"));
    CHECK(SquareMatrix("[[1,2,3][4,5,6][7,8,9]]") - SquareMatrix("[[4,5,7][8,6,9][1,1,2]]") == SquareMatrix("[[-3,-3,-4][-4,-1,-3][6,7,7]]"));
    CHECK(SquareMatrix("[[1,2,3][4,5,6][7,8,9]]") * SquareMatrix("[[4,5,7][8,6,9][1,1,2]]") == SquareMatrix("[[23,20,31][62,56,85][101,92,139]]"));

    #define M_SHOULD_THROW(param1, param2) CHECK_THROWS_AS(SquareMatrix(param1) + SquareMatrix(param2), std::invalid_argument); \
                                           CHECK_THROWS_AS(SquareMatrix(param1) - SquareMatrix(param2), std::invalid_argument); \
                                           CHECK_THROWS_AS(SquareMatrix(param1) * SquareMatrix(param2), std::invalid_argument)

    M_SHOULD_THROW("[[1,2][3,4]]", "[[5]]");
    M_SHOULD_THROW("[[1,2,3][3,4,5][3,4,5]]", "[[5,2][5,3]]");

    #undef M_SHOULD_THROW
}

TEST_CASE("SquareMatrix constructors", "[SquareMatrix]")
{
    for (const std::string &s : g_testStrings)
        CHECK_NOTHROW(SquareMatrix(s));

    CHECK_NOTHROW(SquareMatrix("[[+0,0][0,0]]"));

    #define M_SHOULD_THROW(x) CHECK_THROWS_AS(SquareMatrix(x), std::invalid_argument)

    M_SHOULD_THROW("");
    M_SHOULD_THROW("[]");
    M_SHOULD_THROW("[");
    M_SHOULD_THROW("]");
    M_SHOULD_THROW("[[]");
    M_SHOULD_THROW("[]]");
    M_SHOULD_THROW("[[]]");
    M_SHOULD_THROW("[[],[]]");

    M_SHOULD_THROW("[2]");
    M_SHOULD_THROW("[1,2,3][1,2,3][1,2,3]");
    M_SHOULD_THROW("[[1, 2, 3], [1, 2, 3], [1, 2, 3]]");
    M_SHOULD_THROW("[[1,2,3][1,2,3][1,2,3]] ");
    M_SHOULD_THROW(" [[1,2,3][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[1,2,3,]][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[1,2,3][1,2,3][1,2,3]");
    M_SHOULD_THROW("[1,2,3][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[1,2,3][1,2,3][1,2,3]]a");
    M_SHOULD_THROW("b[[1,2,3][1,2,3][1,2,3]]");
    M_SHOULD_THROW("[[1,2,3] [4,5,6] [7,8,9]]");
    M_SHOULD_THROW("[[1,2,3],[4,5,6],[7,8,9]]");
    M_SHOULD_THROW("[[0.0,0][0,0]]");
    M_SHOULD_THROW("[[0.2,0][0,0]]");
    M_SHOULD_THROW("[[1.2,0][0,0]]");
    M_SHOULD_THROW("[[--0,0][0,0]]");
    M_SHOULD_THROW("[[+-0,0][0,0]]");
    M_SHOULD_THROW("[[1,2][3,4]][[1,2][3,4]]");
    M_SHOULD_THROW("[[a,2][3,4]]");
    M_SHOULD_THROW("[[a,b][c,d]]");
    M_SHOULD_THROW("[[0x1C9,2][3,4]]");
    M_SHOULD_THROW("((1,2)(4,5))");
    M_SHOULD_THROW("[[1 2][4 5]]");
    M_SHOULD_THROW("[[1;2][4;5]]");
    M_SHOULD_THROW("[[1-2][4-5]]");
    M_SHOULD_THROW("[[1,\n2,3][1,2,3][1,2,3]]");

    // Incorrect spacing.
    M_SHOULD_THROW("[[1, 2, 3][1, 2, 3][1, 2, 3]]");

    // Non-square matrices
    M_SHOULD_THROW("[[1,2,3][2,3,4][5,4,2][2,3,4]]");
    M_SHOULD_THROW("[[1,2][3,4,5]]");
    M_SHOULD_THROW("[[1,2]]");

    // Overflow (for reference, 9223372036854775807 is the maximum value for a signed 64-bit integer)
    M_SHOULD_THROW("[[1,2][4,59999999999999999999999999999999999]]");
    M_SHOULD_THROW("[[1,2][4,-59999999999999999999999999999999999]]");

    #undef M_SHOULD_THROW

    // Copy constructor
    SquareMatrix a("[[1,2][3,4]]");
    CHECK(SquareMatrix(a) == a);

    // Copy assignment
    SquareMatrix b;
    b = a;
    CHECK(a == b);

    SquareMatrix b2("[[1,2,3][4,5,6][7,8,9]]");
    b2 = a;
    CHECK(b2 == a);

    // Move constructor
    SquareMatrix c(std::move(a));
    CHECK(c == b);

    // Move assignment
    SquareMatrix d;
    d = std::move(b);
    CHECK(c == d);

    SquareMatrix d2("[[1,2,3][4,5,6][7,8,9]]");
    d2 = std::move(d);
    CHECK(c == d2);

    // A negative test case for the comparison operator:
    CHECK_FALSE(SquareMatrix("[[1,2][3,4]]") == SquareMatrix("[[2,3][4,5]]"));
    CHECK_FALSE(SquareMatrix("[[1,2][3,4]]") == SquareMatrix("[[1]]"));
}

TEST_CASE("SquareMatrix transpose", "[SquareMatrix]")
{
    std::vector<std::pair<std::string, std::string>> test = {{"[[1,2][3,4]]", "[[1,3][2,4]]"},
                                                             {"[[1]]", "[[1]]"},
                                                             {"[[1,2,3][4,5,6][7,8,9]]", "[[1,4,7][2,5,8][3,6,9]]"}};

    for (const auto &p : test)
        CHECK(SquareMatrix(p.first).transpose() == SquareMatrix(p.second));
}
